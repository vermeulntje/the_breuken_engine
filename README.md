# README #

A engine based of the minigin engine of DAE.
And the remake of the game bubble bobble (arcade) using this engine.

- Design choises:
The engine has a update loop that splits into 3 seperate functions when it reaches the gameobjects.
The core update which handles all engine related stuff.
The update which is used like one normally would use an update.
And the late update made as a way to prevent the use of unchanged data(collision), and a direct alternative to bufferign of data.
They get called in this order.
The observer pattern is used as an easy way to notify other parts of the code to execute or set states.
The commands pattern is used for inputs but can also be used as a way to simple execute a piece of code.
The Engine has a state system for the character, it keeps track of the state that the character find itself in overall and set sprites accordingly.
 

- Controls:
A and D keys are for moving left and right. (For controller d-pad left, right)
W for jumping. (For controller d-pad up)
F2 and F3 keys are used to swap between different levels.

- BitBucket source link:
https://bitbucket.org/vermeulntje/the_breuken_engine/src/master/
- BitBucket bin link:
https://bitbucket.org/vermeulntje/the_breuken_engine/src/master/bin/

- By: Vermeulen Sebastien - 2DAE02 - 2019/2020
Minigin provided by DAE
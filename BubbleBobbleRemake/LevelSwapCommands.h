#pragma once
#include "BaseCommand.h"
//#include <string>

namespace breuk
{
	class Scene;
}

class AdvanceLevelCommand final : public breuk::BaseCommand
{
public:
	AdvanceLevelCommand();
	virtual ~AdvanceLevelCommand();

	AdvanceLevelCommand(AdvanceLevelCommand& other) = delete;
	AdvanceLevelCommand(AdvanceLevelCommand&& other) = delete;
	AdvanceLevelCommand operator=(AdvanceLevelCommand& other) = delete;
	AdvanceLevelCommand& operator=(AdvanceLevelCommand&& other) = delete;

	virtual void Execute() override;

	void OpenNextLevel() const;

private:
	static const std::string m_BaseSceneName;
	static const std::string m_SceneName;
	static const std::string m_DataTag;
};

class ReturnLevelCommand final : public breuk::BaseCommand
{
public:
	ReturnLevelCommand();
	virtual ~ReturnLevelCommand();

	ReturnLevelCommand(ReturnLevelCommand& other) = delete;
	ReturnLevelCommand(ReturnLevelCommand&& other) = delete;
	ReturnLevelCommand operator=(ReturnLevelCommand& other) = delete;
	ReturnLevelCommand& operator=(ReturnLevelCommand&& other) = delete;

	virtual void Execute() override;

	void OpenPrevLevel() const;

private:
	static const std::string m_BaseSceneName;
	static const std::string m_SceneName;
	static const std::string m_DataTag;
};

class Createplayer1Command final : public breuk::BaseCommand
{
public:
	Createplayer1Command();
	virtual ~Createplayer1Command();

	Createplayer1Command(Createplayer1Command& other) = delete;
	Createplayer1Command(Createplayer1Command&& other) = delete;
	Createplayer1Command operator=(Createplayer1Command& other) = delete;
	Createplayer1Command& operator=(Createplayer1Command&& other) = delete;

	virtual void Execute() override;

	inline void SetScene(breuk::Scene& scene) 
	{
		m_pScene = &scene;
	}

private:
	breuk::Scene* m_pScene;
};

#include "TheBreukenEnginePCH.h"
#include "LevelManager.h"

LevelManager::~LevelManager()
{
}

int LevelManager::AdvanceLevel()
{
	if (m_Nrlevels > m_CurrentLevel)
	{
		m_CurrentLevel++;
	}
	return m_CurrentLevel;
}
int LevelManager::DecreaseLevel()
{
	if (1 < m_CurrentLevel)
	{
		m_CurrentLevel--;
	}
	return m_CurrentLevel;
}
int LevelManager::GetCurrentLevel() const
{ 
	return m_CurrentLevel; 
}
#include "TheBreukenEnginePCH.h"
#include "Settings.h"
#include "BubbleBobbleGame.h"

#include "ResourceManager.h"
#include "InputManager.h"
#include "Renderer.h"
#include "Logger.h"
#include "ObserverManager.h"
#include "SoundManager.h"

#include "Scene.h"
#include "GameObject.h"
#include "Components.h"
#include "GameComponents.h"
#include "LevelSwapCommands.h"
#include "MovementCommands.h"
#include "LandingObserver.h"
#include "FallThroughObserver.h"

#include <chrono>
#include <future>

BubbleBobbleGame::BubbleBobbleGame() 
	:TheBreukenEngine()
	, m_ResourcePath{ "../Resources/" }
{
}

void BubbleBobbleGame::LoadGame() const
{
	breuk::InputManager& inputMan = breuk::InputManager::GetInstance();
	inputMan.AddCommand(SDL_SCANCODE_F2, new ReturnLevelCommand(), XINPUT_GAMEPAD_LEFT_SHOULDER);
	inputMan.AddCommand(SDL_SCANCODE_F3, new AdvanceLevelCommand(), XINPUT_GAMEPAD_RIGHT_SHOULDER);

	breuk::SceneManager& sceneMan = breuk::SceneManager::GetInstance();

	//Load the basescene, first scene added so is auto set to active scene
	//Contains the data for all the levels
	//Will be returned to for easily starting new levels
	LoadBaseScene();
	sceneMan.Initialize();

	//Open the first level
	AdvanceLevelCommand* pTemp = new AdvanceLevelCommand();
	pTemp->OpenNextLevel();
	SAFE_DELETE(pTemp);

	breuk::ObserverManager::GetInstance().AddObserver(new LandingObserver());
	breuk::ObserverManager::GetInstance().AddObserver(new FallThroughObserver());

	breuk::SoundManager::GetInstance().PlaySoundLooped("../Resources/Soundtrack.wav");
}

void BubbleBobbleGame::LoadBaseScene() const
{
	//----- SCENE -----
	//*****************
	breuk::Scene& scene = breuk::SceneManager::GetInstance().CreateScene("BaseScene");

	//----- OBJECTS -----
	//*******************
	breuk::GameObject* pObject = new breuk::GameObject();
	pObject->GetTransform()->SetPosition(0,0);
	breuk::Texture2D* pBackground = new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("background.jpg"));
	pObject->AddComponent(pBackground);
	scene.Add(pObject);

	pObject = new breuk::GameObject();
	pObject->GetTransform()->SetPosition(216, 180);
	breuk::Texture2D* pLogo = new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("logo.png"));
	pObject->AddComponent(pLogo);
	scene.Add(pObject);

	breuk::GameObject* pCounter = new breuk::GameObject();
	pCounter->GetTransform()->SetPosition(5, 5);
	breuk::Font* pFont = breuk::ResourceManager::GetInstance().LoadFont("Lingua.otf", 18);
	pFont->SetText("0 FPS");
	pFont->SetColor({ 255,255,0 });
	pCounter->AddComponent(pFont);
	pCounter->AddComponent(new breuk::FPSCounter());
	pCounter->AddComponent(new breuk::CounterDisplayObject());
	scene.Add(pCounter);

	breuk::GameObject* pLevelDataObject = new breuk::GameObject();
	LevelParser* pParser = new LevelParser();

	//Threaded file reading/parsing to speed up loading times
	std::future<bool> thread1 = std::async(std::launch::async, &LevelParser::OpenEnemyFile, pParser, "SeperatedEnemyData.dat");
	std::future<bool> thread2 = std::async(std::launch::async, &LevelParser::OpenLevelFile, pParser, "SeperatedLevelData.dat");
	if (!thread1.get())
		breuk::Logger::GetInstance().Log("Enemy file was not opened properly.", breuk::MessageLevel::Error);
	if (!thread2.get())
		breuk::Logger::GetInstance().Log("Level file was not opened properly.", breuk::MessageLevel::Error);

	LevelData* pLevelData = new LevelData(100, 32, 25, 3);
	pParser->ParseEnemies(pLevelData->GetEnemyData(), pLevelData->GetEnemyDataSize());
	pParser->ParseLevels(pLevelData->GetLevelData());
	SAFE_DELETE(pParser);
	pLevelDataObject->AddComponent(pLevelData);
	pLevelDataObject->SetTag("LevelData");
	scene.Add(pLevelDataObject);
}

void BubbleBobbleGame::Run()
{
	//Initialize engine related systems
	Initialize();

	//Making all stack variables local and thus preventing memory leaks
	{
		breuk::Renderer& renderer = breuk::Renderer::GetInstance();
		breuk::SceneManager& sceneManager = breuk::SceneManager::GetInstance();
		breuk::InputManager& input = breuk::InputManager::GetInstance();
		breuk::ResourceManager& resource = breuk::ResourceManager::GetInstance();

		//Load the game
		resource.Init(m_ResourcePath);
		LoadGame();
		sceneManager.Initialize();

		float elapsedSec{};
		bool doContinue = true;
		//Main loop
		while (doContinue)
		{
			const auto currentTime = std::chrono::high_resolution_clock::now();

			//Detect Inputs
			doContinue = input.ProcessInput();
			input.HandleInput();

			//Update Scenes
			sceneManager.Update(elapsedSec);

			//Render Scenes
			renderer.Render();

			elapsedSec = std::chrono::duration<float>(std::chrono::high_resolution_clock::now() - currentTime).count();
		}
	}

	Cleanup();
}

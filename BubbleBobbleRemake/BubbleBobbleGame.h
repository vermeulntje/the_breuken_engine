#pragma once
#include "TheBreukenEngine.h"

class BubbleBobbleGame : public breuk::TheBreukenEngine 
{
public:
	BubbleBobbleGame();
	virtual ~BubbleBobbleGame() = default;

	BubbleBobbleGame(BubbleBobbleGame& other) = delete;
	BubbleBobbleGame(BubbleBobbleGame&& other) = delete;
	BubbleBobbleGame operator=(BubbleBobbleGame& other) = delete;
	BubbleBobbleGame& operator=(BubbleBobbleGame&& other) = delete;

	//virtual void Initialize() override;
	//virtual void Cleanup() override;

	virtual void LoadGame() const override;
	virtual void Run() override;

	void LoadBaseScene() const;

private:
	const std::string m_ResourcePath;
};
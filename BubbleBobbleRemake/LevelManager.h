#pragma once
#include "Singleton.h"

namespace breuk
{
	class GameObject;
}

class LevelManager final : public breuk::Singleton<LevelManager>
{
public:
	~LevelManager();

	LevelManager(LevelManager& other) = delete;
	LevelManager(LevelManager&& other) = delete;
	LevelManager operator=(LevelManager& other) = delete;
	LevelManager& operator=(LevelManager&& other) = delete;

	inline void SetAssembler(breuk::GameObject* pAssembler) { m_pAssembler = pAssembler; }
	inline breuk::GameObject* GetAssembler() const { return m_pAssembler; }

	int AdvanceLevel();
	int DecreaseLevel();
	int GetCurrentLevel() const;

private:
	friend class breuk::Singleton<LevelManager>;
	LevelManager() = default;

	breuk::GameObject* m_pAssembler = nullptr;
	int m_Nrlevels = 100;
	int m_CurrentLevel = 0;
};

// 2DAE02 - Vermeulen Sebastien

#include "TheBreukenEnginePCH.h"
#include "Logger.h"

#if _DEBUG
#include <vld.h>
#endif
#include <exception>

#include "BubbleBobbleGame.h"

int main(int, char* [])
{
    BubbleBobbleGame engine{};
    try 
    {
        engine.Run();
    }
    catch (std::exception& e)
    {
        breuk::Logger::GetInstance().Log("Exception: " + std::string(e.what()), breuk::MessageLevel::Error);
    }

    return 0;
}

#pragma once
#include "Observer.h"

class GameObject;

class LandingObserver final : public breuk::Observer
{
public:
	LandingObserver();
	virtual ~LandingObserver();

	LandingObserver(LandingObserver& other) = delete;
	LandingObserver(LandingObserver&& other) = delete;
	LandingObserver operator=(LandingObserver& other) = delete;
	LandingObserver& operator=(LandingObserver&& other) = delete;

	virtual void OnNotify(breuk::GameObject* pObject, const breuk::Event event) override;
};

#pragma once
#include "Observer.h"

class GameObject;

class FallThroughObserver final : public breuk::Observer
{
public:
	FallThroughObserver();
	virtual ~FallThroughObserver();

	FallThroughObserver(FallThroughObserver& other) = delete;
	FallThroughObserver(FallThroughObserver&& other) = delete;
	FallThroughObserver operator=(FallThroughObserver& other) = delete;
	FallThroughObserver& operator=(FallThroughObserver&& other) = delete;

	virtual void OnNotify(breuk::GameObject* pObject, const breuk::Event event) override;
};

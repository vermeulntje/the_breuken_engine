#include "TheBreukenEnginePCH.h"
#include "Settings.h"
#include "LevelAssembler.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Components.h"
#include "SceneManager.h"
#include "Scene.h"

LevelAssembler::LevelAssembler()
	:breuk::BaseComponent()
	, m_BlockTexturePath{ "BubbleBobbleSmallTiles.png" }
	, m_NrRows{1}
	, m_NrColloms{1}
	, m_TextureRows{10}
	, m_TextureColls{10}
	, m_SpriteWidth{8}
	, m_SpriteHeight{8}
{
}
LevelAssembler::~LevelAssembler()
{
}

bool LevelAssembler::Initialize()
{
	m_Initialised = true;
	return m_Initialised;
}

void LevelAssembler::AssembleBlocks(const std::vector<char>& levelData, const size_t levelID) 
{
	int srcXOffset{ ((int)levelID % m_TextureColls) * m_SpriteWidth };
	int srcYOffset{ ((int)levelID / m_TextureRows) * m_SpriteHeight };
	int width = int(breuk::Settings::m_WindowWidth / (float)m_NrColloms);
	int height = int(breuk::Settings::m_WindowHeight / (float)m_NrRows);

	size_t nrStartBits = levelID * m_NrColloms * m_NrRows + 7;
	size_t start{ nrStartBits / 8 };
	size_t nrEndBits = (size_t)m_NrColloms * (size_t)m_NrRows + 7;
	size_t end{ start + nrEndBits / 8 };

	int nrBoxBesides{};
	int offsetIdx{};
	int verOffsetIdx{};

	for (size_t byteIdx{ start }; byteIdx < end; byteIdx++)
	{
		bool isBlock{};
		for (int shift{}; shift < 8; shift++)
		{
			char shiftf = (0b10000000 >> shift);
			isBlock = (levelData[byteIdx] & shiftf);

			float offsetX{ float(((byteIdx % 100) * 8 + shift) % m_NrColloms) * width };
			float offsetY{ float(((byteIdx % 100) * 8 + shift) / m_NrColloms) * height };

			if (isBlock)
			{
				if (offsetY > FLT_EPSILON)
				{
					if (offsetX < FLT_EPSILON)
					{
						MakeCollsion(offsetIdx, (float)verOffsetIdx, width, height, nrBoxBesides);
						nrBoxBesides = 0;
						offsetIdx = 0;
						verOffsetIdx = 0;
					}
					if (nrBoxBesides == 0)
					{
						offsetIdx = ((byteIdx % 100) * 8 + shift) % m_NrColloms;
						verOffsetIdx = ((byteIdx % 100) * 8 + shift) / m_NrColloms;
					}
					nrBoxBesides++;
				}

				breuk::GameObject* pBlock = new breuk::GameObject();

				breuk::Texture2D* pTemp = pBlock->AddComponent(new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture(m_BlockTexturePath)));
				pBlock->GetTransform()->SetPosition(offsetX, offsetY);

				pTemp->SetSourceRect(srcXOffset, srcYOffset, 8, 8);
				pTemp->SetDestRect(20, 20);

				breuk::SceneManager::GetInstance().GetActiveScene()->Add(pBlock);
			}
			else 
			{
				if (nrBoxBesides > 0)
				{
					MakeCollsion(offsetIdx, (float)verOffsetIdx, width, height, nrBoxBesides);
					nrBoxBesides = 0;
					offsetIdx = 0;
					verOffsetIdx = 0;
				}
			}
		}
	}

	MakeCollsion(offsetIdx, (float)verOffsetIdx, width, height, nrBoxBesides);

	//Large side boxes
	MakeCollsion(0, -0.5f, width, 2 * m_NrRows * height, 2);
	MakeCollsion(m_NrColloms - 2, -0.5, width, 2 * m_NrRows * height, 2);
}

void LevelAssembler::MakeCollsion(const int offsetIdx, const float verOffsetIdx, const int width, const int height, const int nrBoxBesides)
{
	breuk::GameObject* pBlock = new breuk::GameObject();

	breuk::PhysicsComponent* pPhysComp = new breuk::PhysicsComponent();
	pBlock->AddComponent(pPhysComp);
	breuk::RectCollsision* pRectCollComp = new breuk::RectCollsision();
	pRectCollComp->SetColor({ 255,255,0 });
	pRectCollComp->SetRect({ offsetIdx * width, (int)(verOffsetIdx * height), nrBoxBesides * width, height });
	pBlock->AddComponent(pRectCollComp);

	breuk::SceneManager::GetInstance().GetActiveScene()->Add(pBlock);
}

#pragma once
#include "BaseComponent.h"
#pragma warning(push)

namespace breuk 
{
	class GameObject;
	class Texture2D;
}

class LevelAssembler final : public breuk::BaseComponent
{
public:
	LevelAssembler();
	~LevelAssembler();

	virtual bool Initialize() override;

	LevelAssembler(LevelAssembler& other) = delete;
	LevelAssembler(LevelAssembler&& other) = delete;
	LevelAssembler& operator=(LevelAssembler& other) = delete;
	LevelAssembler& operator=(LevelAssembler&& other) = delete;

	void AssembleBlocks(const std::vector<char>& levelData, const size_t levelID);

	inline void SetNrBlocks(const int nrRow, const int nrCollom)
	{
		m_NrRows = nrRow;
		m_NrColloms = nrCollom;
	}

private:
	void MakeCollsion(const int offsetIdx, const float verOffsetIdx, const int width, const int height, const int nrBoxBesides);

	const std::string m_BlockTexturePath;
	const int m_TextureRows;
	const int m_TextureColls;
	const int m_SpriteWidth;
	const int m_SpriteHeight;

	int m_NrRows, m_NrColloms;
};
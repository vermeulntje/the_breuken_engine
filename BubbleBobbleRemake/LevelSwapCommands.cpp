#include "TheBreukenEnginePCH.h"
#include "TheBreukenEngine.h"
#include "LevelSwapCommands.h"
#include "LevelManager.h"
#include "SceneManager.h"
#include "Scene.h"
#include "GameComponents.h"
#include "GameObject.h"
#include "Font.h"
#include "ResourceManager.h"
#include "MovementCommands.h"
#include "InputManager.h"
#include "Settings.h"

#pragma region Advance
const std::string AdvanceLevelCommand::m_BaseSceneName = "BaseScene";
const std::string AdvanceLevelCommand::m_SceneName = "Level_";
const std::string AdvanceLevelCommand::m_DataTag = "LevelData";

AdvanceLevelCommand::AdvanceLevelCommand()
	:BaseCommand()
{
}
AdvanceLevelCommand::~AdvanceLevelCommand() 
{
}

void AdvanceLevelCommand::Execute()
{
	//PRESSED
	if (m_CurrTriggered && !m_PrevTriggered)
	{

	}
	//HELD
	else if (m_CurrTriggered && m_PrevTriggered)
	{

	}
	//RELEASED
	else if (!m_CurrTriggered && m_PrevTriggered)
	{
		OpenNextLevel();
	}
	//NONE
	else return;

	//Set bool for prev state
	m_PrevTriggered = m_CurrTriggered;
}

void AdvanceLevelCommand::OpenNextLevel() const
{
	// MANAGEMENT
	//***********
	LevelManager& levelManager = LevelManager::GetInstance();
	breuk::SceneManager& sceneManager = breuk::SceneManager::GetInstance();

	//Previous scene
	breuk::Scene* pBaseScene = sceneManager.GetScene(m_BaseSceneName);
	int newLevel = levelManager.AdvanceLevel();
	if (sceneManager.GetActiveScene()->GetName() != m_BaseSceneName)
		sceneManager.ClearActiveScene();
	// NEW SCENE
	breuk::Scene& scene = sceneManager.CreateScene(m_SceneName + std::to_string(newLevel));
	sceneManager.Initialize();
	sceneManager.SetActiveScene(m_SceneName + std::to_string(newLevel));

	// OBJECT CREATION
	//****************
	LevelData* pLevelData = pBaseScene->GetObjectWithTag(m_DataTag)->GetComponentOfType<LevelData>();

	breuk::GameObject* pAssemblerObject = new breuk::GameObject();
	LevelAssembler* pLevelAssembler = new LevelAssembler();
	pLevelAssembler->SetNrBlocks(pLevelData->GetNrRows(), pLevelData->GetNrColl());
	pAssemblerObject->AddComponent(pLevelAssembler);
	scene.Add(pAssemblerObject);
	pLevelAssembler->AssembleBlocks(pLevelData->GetLevelData(), (size_t)newLevel - 1);

	breuk::GameObject* pCounter = new breuk::GameObject();
	float counterOffset{ 25 };
	pCounter->GetTransform()->SetPosition(2 * counterOffset, counterOffset);
	breuk::Font* pFont = breuk::ResourceManager::GetInstance().LoadFont("Pixel_NES.otf", 18);
	pFont->SetText("0 FPS");
	pFont->SetColor({ 55,55,55 });
	pCounter->AddComponent(pFont);
	pCounter->AddComponent(new breuk::FPSCounter());
	pCounter->AddComponent(new breuk::CounterDisplayObject());
	scene.Add(pCounter);

	// CHARACTER
	//**********
	Createplayer1Command* pCommand = new Createplayer1Command();
	pCommand->SetScene(scene);
	pCommand->Execute();
	SAFE_DELETE(pCommand);
}
#pragma endregion Advance

#pragma region Return
const std::string ReturnLevelCommand::m_BaseSceneName = "BaseScene";
const std::string ReturnLevelCommand::m_SceneName = "Level_";
const std::string ReturnLevelCommand::m_DataTag = "LevelData";

ReturnLevelCommand::ReturnLevelCommand() 
	:BaseCommand()
{
}
ReturnLevelCommand::~ReturnLevelCommand() 
{
}

void ReturnLevelCommand::Execute() 
{
	//PRESSED
	if (m_CurrTriggered && !m_PrevTriggered)
	{

	}
	//HELD
	else if (m_CurrTriggered && m_PrevTriggered)
	{

	}
	//RELEASED
	else if (!m_CurrTriggered && m_PrevTriggered)
	{
		OpenPrevLevel();
	}
	//NONE
	else return;

	//Set bool for prev state
	m_PrevTriggered = m_CurrTriggered;
}

void ReturnLevelCommand::OpenPrevLevel() const
{
	// MANAGEMENT
	//***********
	LevelManager& levelManager = LevelManager::GetInstance();
	breuk::SceneManager& sceneManager = breuk::SceneManager::GetInstance();

	//Previous scene
	breuk::Scene* pBaseScene = sceneManager.GetScene(m_BaseSceneName);
	
	//Level check
	int current = levelManager.GetCurrentLevel();
	int newLevel = levelManager.DecreaseLevel();
	if (current == newLevel)
		return;

	if (sceneManager.GetActiveScene()->GetName() != m_BaseSceneName)
		sceneManager.ClearActiveScene();
	// NEW SCENE
	breuk::Scene& scene = sceneManager.CreateScene(m_SceneName + std::to_string(newLevel));
	sceneManager.Initialize();
	sceneManager.SetActiveScene(m_SceneName + std::to_string(newLevel));

	// OBJECT CREATION
	//****************
	LevelData* pLevelData = pBaseScene->GetObjectWithTag(m_DataTag)->GetComponentOfType<LevelData>();

	breuk::GameObject* pAssemblerObject = new breuk::GameObject();
	LevelAssembler* pLevelAssembler = new LevelAssembler();
	pLevelAssembler->SetNrBlocks(pLevelData->GetNrRows(), pLevelData->GetNrColl());
	pAssemblerObject->AddComponent(pLevelAssembler);
	scene.Add(pAssemblerObject);
	pLevelAssembler->AssembleBlocks(pLevelData->GetLevelData(), (size_t)newLevel - 1);

	breuk::GameObject* pCounter = new breuk::GameObject();
	float counterOffset{25};
	pCounter->GetTransform()->SetPosition(2 * counterOffset, counterOffset);
	breuk::Font* pFont = breuk::ResourceManager::GetInstance().LoadFont("Pixel_NES.otf", 18);
	pFont->SetText("0 FPS");
	pFont->SetColor({ 55,55,55 });
	pCounter->AddComponent(pFont);
	pCounter->AddComponent(new breuk::FPSCounter());
	pCounter->AddComponent(new breuk::CounterDisplayObject());
	scene.Add(pCounter);

	// CHARACTER
	//**********
	Createplayer1Command* pCommand = new Createplayer1Command();
	pCommand->SetScene(scene);
	pCommand->Execute();
	SAFE_DELETE(pCommand);
}
#pragma endregion Return

Createplayer1Command::Createplayer1Command() 
	:BaseCommand()
	, m_pScene(nullptr)
{
}
Createplayer1Command::~Createplayer1Command()
{
}

void Createplayer1Command::Execute()
{
	// CHARACTER
	//**********
	breuk::GameObject* pCharacter = new breuk::GameObject();
	pCharacter->GetTransform()->SetPosition(56, breuk::Settings::m_WindowHeight - 56);
	breuk::MovementState* pMoveComp = new breuk::MovementState();
	pCharacter->AddComponent(pMoveComp);
#pragma region PlayerSprites
	//RightMoveSprite
	breuk::Sprite* pSprite = new breuk::Sprite();
	pSprite->SetFrameRect({ 0,16,16,16 });
	pSprite->SetFrameTime(0.125f);
	pSprite->SetNrRows(1);
	pSprite->SetNrColls(8);
	pSprite->SetNrFrames(8);
	pMoveComp->AddComponent(pSprite);
	pSprite->SetTexture(pSprite->AddComponent(new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("PlayerSpriteSheet_1.png"))));
	pMoveComp->SetLeftWalkSprite(pSprite);
	//LeftMoveSprite
	pSprite = new breuk::Sprite();
	pSprite->SetFrameRect({ 0,0,16,16 });
	pSprite->SetFrameTime(0.125f);
	pSprite->SetNrRows(1);
	pSprite->SetNrColls(8);
	pSprite->SetNrFrames(8);
	pMoveComp->AddComponent(pSprite);
	pSprite->SetTexture(pSprite->AddComponent(new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("PlayerSpriteSheet_1.png"))));
	pMoveComp->SetRightWalkSprite(pSprite);
	////LeftAttackSprite
	//pSprite = new breuk::Sprite();
	//pSprite->SetFrameRect({0,0,16,16});
	//pSprite->SetFrameTime(0.125f);
	//pSprite->SetNrRows(1);
	//pSprite->SetNrColls(8);
	//pSprite->SetNrFrames(8);
	//pMoveComp->AddComponent(pSprite);
	//pSprite->SetTexture(pSprite->AddComponent(new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("PlayerSpriteSheet_1.png"))));
	//pMoveComp->SetAttackLeftSprite(pSprite);
	////RightAttackSprite
	//pSprite = new breuk::Sprite();
	//pSprite->SetFrameRect({0,0,16,16});
	//pSprite->SetFrameTime(0.125f);
	//pSprite->SetNrRows(1);
	//pSprite->SetNrColls(8);
	//pSprite->SetNrFrames(8);
	//pMoveComp->AddComponent(pSprite);
	//pSprite->SetTexture(pSprite->AddComponent(new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("PlayerSpriteSheet_1.png"))));	
	//pMoveComp->SetAttackRightSprite(pSprite);
	////CaptureSprite
	//pSprite = new breuk::Sprite();
	//pSprite->SetFrameRect({0,0,16,16});
	//pSprite->SetFrameTime(0.125f);
	//pSprite->SetNrRows(1);
	//pSprite->SetNrColls(8);
	//pSprite->SetNrFrames(8);
	//pMoveComp->AddComponent(pSprite);
	//pSprite->SetTexture(pSprite->AddComponent(new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("PlayerSpriteSheet_1.png"))));	
	//pMoveComp->SetCaptureSprite(pSprite);
	//DeadSprite
	pSprite = new breuk::Sprite();
	pSprite->SetFrameRect({ 0,0,16,16 });
	pSprite->SetFrameTime(0.125f);
	pSprite->SetNrRows(1);
	pSprite->SetNrColls(16);
	pSprite->SetNrFrames(16);
	pMoveComp->AddComponent(pSprite);
	pSprite->SetTexture(pSprite->AddComponent(new breuk::Texture2D(breuk::ResourceManager::GetInstance().LoadTexture("PlayerSpriteSheet_1_Death.png"))));
	pMoveComp->SetDeathSprite(pSprite);
#pragma endregion PlayerSprites

	breuk::PhysicsComponent* pPhysComp = new breuk::PhysicsComponent();
	pPhysComp->SetGravity(true);
	pPhysComp->SetStatic(false);
	pCharacter->AddComponent(pPhysComp);
	breuk::RectCollsision* pRectCollComp = new breuk::RectCollsision();
	pRectCollComp->SetColor({ 255,255,0 });
	pRectCollComp->SetRect({ 0,0,32,32 });
	pCharacter->AddComponent(pRectCollComp);
	m_pScene->Add(pCharacter);

	// INPUTS
	//*******
	breuk::InputManager& inputMan = breuk::InputManager::GetInstance();
	inputMan.AddCommand(SDL_SCANCODE_A, new MoveLeftCommand(pCharacter), XINPUT_GAMEPAD_DPAD_LEFT);
	inputMan.AddCommand(SDL_SCANCODE_D, new MoveRightCommand(pCharacter), XINPUT_GAMEPAD_DPAD_RIGHT);
	inputMan.AddCommand(SDL_SCANCODE_W, new JumpCommand(pCharacter), XINPUT_GAMEPAD_DPAD_UP);
}

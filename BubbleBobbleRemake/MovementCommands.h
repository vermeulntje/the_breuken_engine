#pragma once
#include "BaseCommand.h"

namespace breuk 
{
	class GameObject;
}

#pragma region Moving
class MoveRightCommand final : public breuk::BaseCommand 
{
public:
	MoveRightCommand(breuk::GameObject* pEntity);
	virtual ~MoveRightCommand();

	MoveRightCommand(MoveRightCommand& other) = delete;
	MoveRightCommand(MoveRightCommand&& other) = delete;
	MoveRightCommand operator=(MoveRightCommand& other) = delete;
	MoveRightCommand& operator=(MoveRightCommand&& other) = delete;

	virtual void Execute() override;

private:
	breuk::GameObject* m_pEntity;
};
class MoveLeftCommand final : public breuk::BaseCommand 
{
public:
	MoveLeftCommand(breuk::GameObject* pEntity);
	virtual ~MoveLeftCommand();

	MoveLeftCommand(MoveLeftCommand& other) = delete;
	MoveLeftCommand(MoveLeftCommand&& other) = delete;
	MoveLeftCommand operator=(MoveLeftCommand& other) = delete;
	MoveLeftCommand& operator=(MoveLeftCommand&& other) = delete;

	virtual void Execute() override;

private:
	breuk::GameObject* m_pEntity;
};
#pragma endregion Moving

#pragma region Jumping
class JumpCommand final : public breuk::BaseCommand
{
public:
	JumpCommand(breuk::GameObject* pEntity);
	virtual ~JumpCommand();

	JumpCommand(JumpCommand& other) = delete;
	JumpCommand(JumpCommand&& other) = delete;
	JumpCommand operator=(JumpCommand& other) = delete;
	JumpCommand& operator=(JumpCommand&& other) = delete;

	virtual void Execute() override;

private:
	breuk::GameObject* m_pEntity;
};
#pragma endregion Jumping

class TeleportToTopCommand final : public breuk::BaseCommand
{
public:
	TeleportToTopCommand(breuk::GameObject* pEntity);
	virtual ~TeleportToTopCommand();

	TeleportToTopCommand(TeleportToTopCommand& other) = delete;
	TeleportToTopCommand(TeleportToTopCommand&& other) = delete;
	TeleportToTopCommand operator=(TeleportToTopCommand& other) = delete;
	TeleportToTopCommand& operator=(TeleportToTopCommand&& other) = delete;

	virtual void Execute() override;

private:
	breuk::GameObject* m_pEntity;
};

#pragma once
#include "BaseComponent.h"
#include <vector>

namespace breuk
{
	class GameObject;
}

class LevelData final : public breuk::BaseComponent
{
public:
	explicit LevelData(const int nrLevels, const int nrWidthTiles, const int nrHeightTiles, const int enemyDataSize);
	virtual ~LevelData();

	virtual bool Initialize() override;

	LevelData(LevelData& other) = delete;
	LevelData(LevelData&& other) = delete;
	LevelData& operator=(LevelData& other) = delete;
	LevelData& operator=(LevelData&& other) = delete;

	inline int GetNrlevels() const { return m_NrLevels; }
	inline int GetNrRows() const { return m_NrRows; }
	inline int GetNrColl() const { return m_NrCols; }
	inline int GetEnemyDataSize() const { return m_EnemyDataSize; }

	inline std::vector<char>& GetLevelData() { return m_LevelTiles; }
	inline std::vector<std::vector<char>>& GetEnemyData() { return m_EnemyData; }

private:
	int m_NrLevels;
	int m_NrRows;
	int m_NrCols;
	int m_EnemyDataSize;

	std::vector<char> m_LevelTiles;
	std::vector<std::vector<char>> m_EnemyData;
};

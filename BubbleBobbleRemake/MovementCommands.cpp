#include "TheBreukenEnginePCH.h"
#include "MovementCommands.h"
#include "Components.h"
#include "GameObject.h"

#pragma region Moving
MoveRightCommand::MoveRightCommand(breuk::GameObject* pEntity) 
	:BaseCommand()
	, m_pEntity(pEntity)
{
}
MoveRightCommand::~MoveRightCommand() 
{
}

void MoveRightCommand::Execute()
{
	//PRESSED
	if (m_CurrTriggered && !m_PrevTriggered)
	{
		breuk::MovementState* pMovState = m_pEntity->GetComponentOfType<breuk::MovementState>();
		pMovState->StartMovingRight();
	}
	//RELEASED
	else if (!m_CurrTriggered && m_PrevTriggered)
	{
		breuk::MovementState* pMovState = m_pEntity->GetComponentOfType<breuk::MovementState>();
		pMovState->StopMovingRight();
	}

	//Set bool for prev state
	m_PrevTriggered = m_CurrTriggered;
}

MoveLeftCommand::MoveLeftCommand(breuk::GameObject* pEntity) 
	:BaseCommand()
	, m_pEntity(pEntity)
{
}
MoveLeftCommand::~MoveLeftCommand() 
{
}

void MoveLeftCommand::Execute() 
{
	//PRESSED
	if (m_CurrTriggered && !m_PrevTriggered)
	{
		breuk::MovementState* pMovState = m_pEntity->GetComponentOfType<breuk::MovementState>();
		pMovState->StartMovingLeft();
	}
	//RELEASED
	else if (!m_CurrTriggered && m_PrevTriggered)
	{
		breuk::MovementState* pMovState = m_pEntity->GetComponentOfType<breuk::MovementState>();
		pMovState->StopMovingLeft();
	}

	//Set bool for prev state
	m_PrevTriggered = m_CurrTriggered;
}
#pragma endregion Moving

#pragma region Jumping
JumpCommand::JumpCommand(breuk::GameObject* pEntity)
	:BaseCommand()
	, m_pEntity(pEntity)
{
}
JumpCommand::~JumpCommand()
{
}

void JumpCommand::Execute()
{
	//PRESSED
	if (m_CurrTriggered && !m_PrevTriggered)
	{
		breuk::MovementState* pMovState = m_pEntity->GetComponentOfType<breuk::MovementState>();
		pMovState->Jumped();
	}

	//Set bool for prev state
	m_PrevTriggered = m_CurrTriggered;
}
#pragma endregion Jumping

TeleportToTopCommand::TeleportToTopCommand(breuk::GameObject* pEntity) 
	:BaseCommand()
	,m_pEntity(pEntity)
{
}
TeleportToTopCommand::~TeleportToTopCommand()
{
}

void TeleportToTopCommand::Execute() 
{
	breuk::Transform* pTrans = m_pEntity->GetTransform();
	m_pEntity->GetTransform()->SetPosition(pTrans->GetPosition().x, 0);
}

#include "TheBreukenEnginePCH.h"
#include "FallThroughObserver.h"
#include "GameObject.h"
#include "Components.h"
#include "MovementCommands.h"

FallThroughObserver::FallThroughObserver()
	:breuk::Observer()
{
}
FallThroughObserver::~FallThroughObserver()
{
}

void FallThroughObserver::OnNotify(breuk::GameObject* pObject, const breuk::Event event)
{
	if (event == breuk::Event::FallThroughEvent)
	{
		TeleportToTopCommand* pFallThrough = new TeleportToTopCommand(pObject);
		pFallThrough->Execute();
		SAFE_DELETE(pFallThrough);
	}
}
#include "TheBreukenEnginePCH.h"
#include "LandingObserver.h"
#include "GameObject.h"
#include "Components.h"

LandingObserver::LandingObserver() 
	:breuk::Observer()
{
}
LandingObserver::~LandingObserver() 
{
}

void LandingObserver::OnNotify(breuk::GameObject* pObject, const breuk::Event event)
{
	if (event == breuk::Event::LandingEvent) 
	{
		breuk::MovementState* pMoveState = pObject->GetComponentOfType<breuk::MovementState>();
		if (pMoveState)
			pMoveState->Landed();
	}
}
#pragma once
#include "BaseComponent.h"
#include <string>
#include <vector>
#include <fstream>

namespace breuk
{
	class GameObject;
	class Transform;
}

class LevelParser final : public breuk::BaseComponent
{
public:
	explicit LevelParser();
	virtual ~LevelParser();

	LevelParser(LevelParser& other) = delete;
	LevelParser(LevelParser&& other) = delete;
	LevelParser& operator=(LevelParser& other) = delete;
	LevelParser& operator=(LevelParser&& other) = delete;

	virtual bool Initialize() override;

	bool OpenLevelFile(const std::string& fileLoc);
	bool OpenEnemyFile(const std::string& fileLoc);

	bool ParseLevels(std::vector<char>& container);
	bool ParseEnemies(std::vector<std::vector<char>>& container, const int enemyDataSize);

private:
	std::ifstream m_LevelFile;
	std::ifstream m_EnemyFile;
};

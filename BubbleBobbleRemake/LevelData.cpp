#include "TheBreukenEnginePCH.h"
#include "LevelData.h"
#include "GameObject.h"
#include "LevelParser.h"
#include "ResourceManager.h"

LevelData::LevelData(const int nrLevels, const int nrWidthTiles, const int nrHeightTiles, const int enemyDataSize)
	:breuk::BaseComponent()
	, m_NrLevels{ nrLevels }
	, m_NrRows{ nrHeightTiles }
	, m_NrCols{nrWidthTiles}
	, m_EnemyDataSize{enemyDataSize}
	, m_LevelTiles{ }
	, m_EnemyData{ }
{
	m_LevelTiles.resize(m_NrRows * m_NrCols * m_NrLevels / 8);
	m_EnemyData.resize(m_NrLevels);
}
LevelData::~LevelData()
{
}

bool LevelData::Initialize()
{
	m_Initialised = true;
	return m_Initialised;
}

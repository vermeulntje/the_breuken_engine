#pragma once
#include "BaseComponent.h"

namespace breuk 
{
	class PhysicsComponent final : public BaseComponent
	{
	public:
		PhysicsComponent();
		virtual ~PhysicsComponent();

		PhysicsComponent(PhysicsComponent& other) = delete;
		PhysicsComponent(PhysicsComponent&& other) = delete;
		PhysicsComponent operator=(PhysicsComponent& other) = delete;
		PhysicsComponent& operator=(PhysicsComponent&& other) = delete;

		virtual bool Initialize() override;
		virtual void Update(const float deltaT) override;
		virtual void LateUpdate(const float deltaT) override;

		void DecideBlock(const glm::vec2& blockNormDirX, const glm::vec2& blockNormDirY);

		inline void AddBaseMovement(const glm::vec2& dir) 
		{
			m_MovementDistance += dir;
		}
		inline void AddVelocity(const glm::vec2& dir) 
		{ 
			m_MovementVelocity += dir; 
		}
		inline void AddForce(const glm::vec2& dir)
		{
			m_MovementForce += dir;
		}
		inline void SetVelocity(const glm::vec2& dir) 
		{ 
			m_MovementVelocity = dir; 
		}
		inline glm::vec2 GetVelocity() const 
		{
			return m_MovementVelocity;
		}
		inline void SetGravity(const bool works) 
		{
			m_HasGravity = works;
		}		
		inline void SetStatic(const bool isStatic) 
		{
			m_IsStatic = isStatic;
		}
		inline bool IsStatic() const 
		{
			return m_IsStatic;
		}

	private:
		glm::vec2 m_MovementDistance;
		glm::vec2 m_MovementVelocity;
		glm::vec2 m_MovementForce;

		float m_TerminalVelocity;
		float m_Gravity;

		float m_UpDownTime;
		float m_LeftRightTime;

		bool m_HasGravity;
		bool m_HasCollDown;
		bool m_HasCollUp;
		bool m_HasCollLeft;
		bool m_HasCollRight;

		bool m_IsStatic;
	};
}
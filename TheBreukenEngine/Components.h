#pragma once
//Base component
#include "BaseComponent.h"
//Components
#include "Texture2D.h"
#include "Transform.h"
#include "Font.h"
#include "FPSCounter.h"
#include "RectCollision.h"
#include "PhysicsComponent.h"
#include "Sprite.h"
#include "MovementState.h"
#include "CounterDisplayObject.h"

#include "TheBreukenEnginePCH.h"
#include "ObserverManager.h"
#include "GameObject.h"

breuk::ObserverManager::ObserverManager() 
	:m_Observers()
{
}
breuk::ObserverManager::~ObserverManager() 
{
	for (size_t idx{}; idx < m_Observers.size(); idx++)
	{
		SAFE_DELETE(m_Observers[idx]);
	}
}

void breuk::ObserverManager::Notify(breuk::GameObject* pObject, const breuk::Event event)
{
	for (size_t idx{}; idx < m_Observers.size(); idx++)
	{
		m_Observers[idx]->OnNotify(pObject, event);
	}
}

void breuk::ObserverManager::AddObserver(breuk::Observer* pObserver) 
{
	for (size_t idx{}; idx < m_Observers.size(); idx++)
	{
		if (m_Observers[idx] == pObserver) return;
	}
	m_Observers.push_back(pObserver);
}
void breuk::ObserverManager::RemoveObserver(breuk::Observer* pObserver) 
{
	for (size_t idx{}; idx < m_Observers.size(); idx++)
	{
		if (m_Observers[idx] == pObserver) 
		{
			SAFE_DELETE(m_Observers[idx]);
			m_Observers.erase(m_Observers.begin() + idx);
		}
	}
}

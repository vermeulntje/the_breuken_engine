#include "TheBreukenEnginePCH.h"
#include "RectCollision.h"
#include "DebugRenderer.h"
#include "Components.h"
#include "GameObject.h"
#include "CollisionManager.h"

#include <algorithm>

breuk::RectCollsision::RectCollsision()
    :BaseComponent()
    , m_Rect({0, 0, 0, 0})
    , m_Color({ 0,0,0 })
{
}
breuk::RectCollsision::~RectCollsision()
{
}

bool breuk::RectCollsision::Initialize()
{
    m_Initialised = true;
    return m_Initialised;
}
void breuk::RectCollsision::LateDraw() const
{
#ifdef _DEBUG
    glm::vec2 pos = m_pParent->GetTransform()->GetPosition();
    SDL_Rect rect = { m_Rect.x + (int)pos.x, m_Rect.y + (int)pos.y, m_Rect.w, m_Rect.h };
    breuk::DebugRenderer::GetInstance().DrawEmptyRect(rect, m_Color);
#endif
}

//Info gotten from
//https://amanotes.com/using-swept-aabb-to-detect-and-process-collision/
bool breuk::RectCollsision::IsColliding(const SDL_Rect& src, const SDL_Rect& dest)
{
    float left = float(dest.x - (src.x + src.w));
    float top = float((dest.y + dest.h) - src.y);
    float right = float((dest.x + dest.w) - src.x);
    float bottom = float(dest.y - (src.y + src.h));

    return !(left > FLT_EPSILON || right < -FLT_EPSILON || top < -FLT_EPSILON || bottom > FLT_EPSILON);
}

SDL_Rect breuk::RectCollsision::GetSweptBroadphaseRect(const SDL_Rect& object, const glm::vec2& movementVelocity)
{
    int x = int((movementVelocity.x > FLT_EPSILON) ? object.x : object.x + movementVelocity.x);
    int y = int((movementVelocity.y > FLT_EPSILON) ? object.y : object.y + movementVelocity.y);
    int w = int(object.w + abs(movementVelocity.x));
    int h = int(object.h + abs(movementVelocity.y));

    return SDL_Rect{ x,y,w,h };
}

float breuk::RectCollsision::SweptAABB(const SDL_Rect& object, const SDL_Rect& other, glm::vec2& collisionDir, const glm::vec2& movementVelocity)
{
    breuk::PhysicsComponent* pPhysComp = m_pParent->GetComponentOfType<breuk::PhysicsComponent>();
    if (!pPhysComp) return 1.f;

    glm::vec2 fixedVel = movementVelocity;
    fixedVel.y = fixedVel.y;

    SDL_Rect rect = GetSweptBroadphaseRect(object, fixedVel);
    if (!IsColliding(rect, other)) return 1.f;

    float dxEntry{}, dxExit{};
    float dyEntry{}, dyExit{};

    if (fixedVel.x > 0.0f)
    {
        dxEntry = float(other.x - (object.x + object.w));
        dxExit = float((other.x + other.w) - object.x);
    }
    else
    {
        dxEntry = float((other.x + other.w) - object.x);
        dxExit = float(other.x - (object.x + object.w));
    }

    if (fixedVel.y > 0.0f)
    {
        dyEntry = float(other.y - (object.y + object.h));
        dyExit = float((other.y + other.h) - object.y);
    }
    else
    {
        dyEntry = float((other.y + other.h) - object.y);
        dyExit = float(other.y - (object.y + object.h));
    }

    float txEntry{}, txExit{};
    float tyEntry{}, tyExit{};

    if (fixedVel.x == 0.0f)
    {
        txEntry = -std::numeric_limits<float>::infinity();
        txExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        txEntry = dxEntry / fixedVel.x;
        txExit = dxExit / fixedVel.x;
    }

    if (fixedVel.y == 0.0f)
    {
        tyEntry = -std::numeric_limits<float>::infinity();
        tyExit = std::numeric_limits<float>::infinity();
    }
    else
    {
        tyEntry = dyEntry / fixedVel.y;
        tyExit = dyExit / fixedVel.y;
    }

    float entryTime = std::max<float>(txEntry, tyEntry);
    float exitTime = std::min<float>(txExit, tyExit);

    if (entryTime > exitTime || (txEntry < 0.f && tyEntry < 0.f) || txEntry > 1.f || tyEntry > 1.f)
    {
        return 1.f;
    }

    if (dxEntry > 0)
        collisionDir.x = 1;
    else
        collisionDir.x = -1;
    if (dyEntry > 0.f)
        collisionDir.y = 1;
    else
    {
        collisionDir.y = -1;
        if (object.y > other.y)
        {
            collisionDir.y = 1;
        }
    }

    return entryTime;
}

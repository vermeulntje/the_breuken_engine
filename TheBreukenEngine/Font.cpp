#include "TheBreukenEnginePCH.h"
#include "Font.h"
#include "Renderer.h"
#include "GameObject.h"

breuk::Font::Font(const std::string& fullPath, unsigned int size)
	:breuk::BaseComponent()
	, m_pFont(nullptr)
	, m_Size(size)
	, m_Text("empty")
	, m_Path(fullPath)
	, m_pLocalTransform()
	, m_Color({255,255,255})
	, m_NeedsUpdate(true)
	, m_pTexture()
	, m_SrcTopFrac(0)
	, m_SrcLeftFrac(0)
	, m_SrcWidthFrac(1)
	, m_SrcHeightFrac(1)
{
}

breuk::Font::~Font()
{
	TTF_CloseFont(m_pFont);
}

bool breuk::Font::Initialize()
{
	m_pFont = TTF_OpenFont(m_Path.c_str(), m_Size);
	if (m_pFont == nullptr)
		throw std::runtime_error(std::string("Failed to load font: ") + SDL_GetError());

	m_pLocalTransform = AddComponent(new breuk::Transform());
	m_pTexture = AddComponent(new breuk::Texture2D(nullptr));

	m_Initialised = true;
	return m_Initialised;
}

void breuk::Font::Draw() const
{
	breuk::Transform* pTransform = static_cast<breuk::Transform*>(m_pParent->GetComponentOfType<breuk::Transform>());
	const glm::vec2& pos = pTransform->GetPosition();
	const glm::vec2& localPos = m_pLocalTransform->GetPosition();

	SDL_Rect dest{};
	dest.x = int(pos.x + localPos.x);
	dest.y = int(pos.y + localPos.y);
	SDL_QueryTexture(m_pTexture->GetSDLTexture(), nullptr, nullptr, &dest.w, &dest.h);
	SDL_Rect src{};
	src.x = int(m_SrcLeftFrac * dest.w);
	src.y = int(m_SrcTopFrac * dest.h);
	src.w = int(m_SrcWidthFrac * dest.w);
	src.h = int(m_SrcHeightFrac * dest.h);
	breuk::Renderer::GetInstance().RenderTexture(*m_pTexture, src, dest);
}

void breuk::Font::Update(const float elapsedSec)
{
	UNREFERENCED_PARAMETER(elapsedSec);

	if (m_NeedsUpdate)
	{
		SDL_Surface* surf = TTF_RenderText_Blended(m_pFont, m_Text.c_str(), m_Color);
		if (surf == nullptr)
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		SDL_Texture* texture = SDL_CreateTextureFromSurface(breuk::Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);
		m_pTexture->SetSDLTexture(texture);
		m_NeedsUpdate = false;
	}
}


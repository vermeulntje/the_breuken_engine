#include "TheBreukenEnginePCH.h"
#include "CollisionManager.h"

breuk::CollisionManager::CollisionManager() 
	:m_CollisionComponents()
{

}
breuk::CollisionManager::~CollisionManager() 
{

}

void breuk::CollisionManager::AddCollisionComp(breuk::BaseComponent* pComponent) 
{
	for (size_t idx{}; idx < m_CollisionComponents.size(); idx++)
	{
		if (m_CollisionComponents[idx] == pComponent)
			return;
	}
	m_CollisionComponents.push_back(pComponent);
}

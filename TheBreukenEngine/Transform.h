#pragma once
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#pragma warning(pop)

#include "BaseComponent.h"

namespace breuk
{
	class GameObject;

	class Transform final : public breuk::BaseComponent
	{
	public:
		Transform();
		virtual ~Transform();

		Transform(Transform& other) = delete;
		Transform(Transform&& other) = delete;
		Transform operator=(Transform& other) = delete;
		Transform& operator=(Transform&& other) = delete;

		virtual bool Initialize() override;

		const glm::vec2& GetPosition() const { return m_Position; }
		void SetPosition(float x, float y);
	private:
		glm::vec2 m_Position;
	};
}

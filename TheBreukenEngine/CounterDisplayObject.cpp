#include "TheBreukenEnginePCH.h"
#include "CounterDisplayObject.h"
#include "GameObject.h"
#include "Components.h"

breuk::CounterDisplayObject::CounterDisplayObject() 
	:BaseComponent()
{
}
breuk::CounterDisplayObject::~CounterDisplayObject() 
{
}

bool breuk::CounterDisplayObject::Initialize()
{
	m_Initialised = true;
	return m_Initialised;
}
void breuk::CounterDisplayObject::Update(const float deltaT)
{
	UNREFERENCED_PARAMETER(deltaT);

	breuk::FPSCounter* pCounter = m_pParent->GetComponentOfType<breuk::FPSCounter>();
	if (pCounter)
	{
		int count{};
		if (pCounter->CheckTime(count))
		{
			std::string fps{ std::to_string(count) + " FPS" };

			breuk::Font* pFont = m_pParent->GetComponentOfType<breuk::Font>();
			pFont->SetText(fps);
		}
	}
}

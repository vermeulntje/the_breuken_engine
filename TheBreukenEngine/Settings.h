#pragma once
namespace breuk
{
	struct Settings
	{
		static const int MsPerFrame = 16; //16 for 60 fps, 33 for 30 fps
		static const int m_WindowWidth = 640;
		static const int m_WindowHeight = 500;
	};
}
#pragma once
#include "SceneManager.h"

namespace breuk
{
	class GameObject;
	class BaseComponent;

	class Scene
	{
	public:
		explicit Scene(const std::string& name);
		~Scene();

		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;

		// CORE
		//*****
		bool Initialize();
		void Update(const float elapsedSec);
		void Render() const;

		void InitObjects();

		void Add(GameObject* object);

		inline std::string GetName() const 
		{
			return m_Name;
		}

		//************
		// TAG GETTERS
		GameObject* GetObjectWithTag(const std::string& tag);
		GameObject* GetObjectWithTag(const std::string& tag) const;
		std::vector<GameObject*> GetAllObjectsWithTag(const std::string& tag);
		std::vector<GameObject*> GetAllObjectsWithTag(const std::string& tag) const;

		//*************
		// TYPE GETTERS
		inline std::vector<GameObject*> GetAllObjects()
		{
			return m_InitObjects;
		}

	private: 
		std::string m_Name;
		std::vector<GameObject*> m_UnInitObjects{};
		std::vector<GameObject*> m_InitObjects{};

		static unsigned int m_IdCounter; 
	};

}

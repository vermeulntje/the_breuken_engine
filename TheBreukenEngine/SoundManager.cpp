#include "TheBreukenEnginePCH.h"
#include "SoundManager.h"
#include <Windows.h>
#include <mmsystem.h>

breuk::SoundManager::SoundManager() 
{
}
breuk::SoundManager::~SoundManager() 
{
}

void breuk::SoundManager::PlaySoundLooped(const std::string& file)
{
    PlaySound(file.c_str(), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
}

#include "TheBreukenEnginePCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "Components.h"

breuk::GameObject::GameObject(const std::string& tag)
	:m_Tag{tag}
	, m_UnInitComponents()
	, m_InitComponents()
{
	m_pTransform = new breuk::Transform();
	m_InitComponents.push_back(m_pTransform);
}

breuk::GameObject::~GameObject() 
{
	for (breuk::BaseComponent* pComponent : m_UnInitComponents)
	{
		SAFE_DELETE(pComponent);
	}
	for (breuk::BaseComponent* pComponent : m_InitComponents)
	{
		SAFE_DELETE(pComponent);
	}
}

// CORE
//*****
bool breuk::GameObject::Initialize()
{
	InitComponents();
	return true;
}
void breuk::GameObject::CoreUpdate(const float elapsedSec)
{
	//Try and initialize new components
	InitComponents();

	for (size_t idx{}; idx < m_InitComponents.size(); idx++)
	{
		m_InitComponents[idx]->CoreUpdate(elapsedSec);
	}
}
void breuk::GameObject::Update(const float elapsedSec)
{
	for (size_t idx{}; idx < m_InitComponents.size(); idx++)
	{
		m_InitComponents[idx]->Update(elapsedSec);
	}
}
void breuk::GameObject::LateUpdate(const float elapsedSec)
{
	//Update the initialized ones
	for (size_t idx{}; idx < m_InitComponents.size(); idx++)
	{
		m_InitComponents[idx]->LateUpdate(elapsedSec);
	}
}
void breuk::GameObject::Render() const
{
	//Only render initialized components
	for (size_t idx{}; idx < m_InitComponents.size(); idx++)
	{
		m_InitComponents[idx]->Draw();
	}
}
void breuk::GameObject::LateRender() const
{
	//Only render initialized components
	for (size_t idx{}; idx < m_InitComponents.size(); idx++)
	{
		m_InitComponents[idx]->LateDraw();
	}
}

void breuk::GameObject::InitComponents()
{
	//Try and initialize new components
	for (size_t idx{}; idx < m_UnInitComponents.size(); idx++)
	{
		if (m_UnInitComponents[idx]->Initialize())
		{
			m_InitComponents.push_back(m_UnInitComponents[idx]);
			m_UnInitComponents.erase(m_UnInitComponents.begin() + idx);
			idx--;
		}
	}
}

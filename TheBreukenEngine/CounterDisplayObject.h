#pragma once
#include "BaseComponent.h"

namespace breuk
{
	class CounterDisplayObject final : public breuk::BaseComponent
	{
	public:
		CounterDisplayObject();
		virtual ~CounterDisplayObject();

		CounterDisplayObject(CounterDisplayObject& other) = delete;
		CounterDisplayObject(CounterDisplayObject&& other) = delete;
		CounterDisplayObject operator=(CounterDisplayObject& other) = delete;
		CounterDisplayObject& operator=(CounterDisplayObject&& other) = delete;

		virtual bool Initialize() override;
		virtual void Update(const float deltaT) override;
	};
}

#pragma once
//#include "Transform.h"

namespace breuk
{
	class GameObject;
	class Transform;

	class BaseComponent
	{
	public:
		explicit BaseComponent();
		virtual ~BaseComponent();

		BaseComponent(const BaseComponent&) = delete;
		BaseComponent(BaseComponent&&) = delete;
		BaseComponent& operator= (const BaseComponent&) = delete;
		BaseComponent& operator= (const BaseComponent&&) = delete;

		// CORE
		//*****
		virtual bool CoreInitialize();
		virtual bool Initialize() = 0;
		virtual void Draw() const;
		virtual void LateDraw() const;
		virtual void CoreUpdate(const float deltaT);
		virtual void Update(const float deltaT);
		virtual void LateUpdate(const float deltaT);

		virtual void InitComponents();

		template<class T>
		inline T* AddComponent(T* pComponent)
		{
			for (breuk::BaseComponent* component : m_UnInitChildComponents)
			{
				//Components was already added
				if (component == pComponent)
					return nullptr;
			}		
			for (breuk::BaseComponent* component : m_InitChildComponents)
			{
				//Components was already added
				if (component == pComponent)
					return nullptr;
			}
			//No extra transform component allowed
			if (typeid(pComponent) == typeid(breuk::Transform))
				return nullptr;

			m_UnInitChildComponents.push_back(pComponent);
			static_cast<breuk::BaseComponent*>(pComponent)->SetParent(m_pParent);
			return pComponent;
		}

		inline virtual void SetParent(breuk::GameObject* pParent) 
		{ 
			m_pParent = pParent; 
		}

		//*************
		// TYPE GETTERS
		inline virtual std::vector<BaseComponent*> GetComponents() 
		{ 
			return m_InitChildComponents;
		}
		template<class T>
		inline T* GetComponentOfType()
		{
			for (breuk::BaseComponent* component : m_InitChildComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					return cast;
			}
			return nullptr;
		}
		template<class T>
		inline T* GetComponentOfType() const
		{
			for (breuk::BaseComponent* component : m_InitChildComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					return cast;
			}
			return nullptr;
		}
		template<class T>
		inline std::vector<T*> GetAllComponentOfType()
		{
			std::vector<T*> temp{};
			for (breuk::BaseComponent* component : m_InitChildComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					temp.push_back(temp);
			}
			return temp;
		}
		template<class T>
		inline std::vector<T*> GetAllComponentOfType() const
		{
			std::vector<T*> temp{};
			for (breuk::BaseComponent* component : m_InitChildComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					temp.push_back(temp);
			}
			return temp;
		}

	protected:
		breuk::GameObject* m_pParent;
		std::vector<breuk::BaseComponent*> m_UnInitChildComponents;
		std::vector<breuk::BaseComponent*> m_InitChildComponents;
		bool m_Initialised;
	};
}

#pragma once
#include "BaseComponent.h"

namespace breuk 
{
	class Texture2D;

	class Sprite final : public breuk::BaseComponent
	{
	public:
		Sprite();
		virtual ~Sprite();

		Sprite(Sprite& other) = delete;
		Sprite(Sprite&& other) = delete;
		Sprite operator=(Sprite& other) = delete;
		Sprite& operator=(Sprite&& other) = delete;

		virtual bool Initialize() override;
		virtual void Update(const float elapsedSec) override;

		inline void SetTexture(Texture2D* pTexture)
		{
			m_pTexture = pTexture;
		}
		inline void SetFrameRect(const SDL_Rect& frame)
		{
			m_FrameRect = frame;
		}
		inline void SetNrRows(const int rows) 
		{
			m_NrFrameRows = rows;
		}
		inline void SetNrColls(const int colls) 
		{
			m_NrFrameColls = colls;
		}
		inline void SetNrFrames(const int nrFrames) 
		{
			m_NrFrames = nrFrames;
		}
		inline void SetFrameTime(const float frameTime) 
		{
			m_FrameTime = frameTime;
		}

	private:
		Texture2D* m_pTexture;

		SDL_Rect m_FrameRect;

		float m_FrameTime;
		float m_CurrentFrameTime;

		int m_NrFrames;
		int m_CurrentFrame;
		int m_NrFrameColls;
		int m_NrFrameRows;
	};
}
#include "TheBreukenEnginePCH.h"
#include "PhysicsComponent.h"
#include "Components.h"
#include "GameObject.h"
#include "SceneManager.h"
#include "Scene.h"
#include "ObserverManager.h"
#include "Settings.h"

breuk::PhysicsComponent::PhysicsComponent()
	:BaseComponent()
	, m_MovementVelocity(0, 0)
	, m_UpDownTime(0)
	, m_LeftRightTime(0)
	, m_HasCollDown(false)
	, m_HasCollLeft(false)
	, m_HasCollRight(false)
	, m_HasCollUp(false)
	, m_Gravity(500.f)
	, m_TerminalVelocity(300.f)
	, m_HasGravity(false)
	, m_IsStatic(true)
	, m_MovementForce(0,0)
{
}
breuk::PhysicsComponent::~PhysicsComponent() 
{
}

bool breuk::PhysicsComponent::Initialize() 
{
	m_Initialised = true;
	return m_Initialised;
}

void breuk::PhysicsComponent::Update(const float deltaT) 
{
	if (m_IsStatic) return;

	m_UpDownTime = 1.f;
	m_LeftRightTime = 1.f;

	if (((m_MovementForce.y > FLT_EPSILON) && m_HasCollDown) || ((m_MovementForce.y < -FLT_EPSILON) && m_HasCollUp))
		m_MovementForce.y = 0.f;
	if (((m_MovementForce.x < -FLT_EPSILON) && m_HasCollLeft) || ((m_MovementForce.x > FLT_EPSILON) && m_HasCollRight))
		m_MovementForce.x = 0.f;

	m_HasCollDown = false;
	m_HasCollUp = false;
	m_HasCollLeft = false;
	m_HasCollRight = false;

	if (m_HasGravity)
	{
		m_MovementForce.y += m_Gravity * deltaT;
	}

	if (m_MovementForce.x > m_TerminalVelocity)
		m_MovementForce.x = m_TerminalVelocity;
	if (m_MovementForce.y > m_TerminalVelocity)
		m_MovementForce.y = m_TerminalVelocity;

	m_MovementVelocity.x = m_MovementForce.x * deltaT;
	m_MovementVelocity.y = m_MovementForce.y * deltaT;
}

//Source
//https://amanotes.com/using-swept-aabb-to-detect-and-process-collision/
void breuk::PhysicsComponent::LateUpdate(const float deltaT)
{
	UNREFERENCED_PARAMETER(deltaT);
	if (m_IsStatic) return;

	breuk::RectCollsision* pRectColl = m_pParent->GetComponentOfType<breuk::RectCollsision>();
	if (!pRectColl) return;

	std::vector<breuk::GameObject*> pObjects = breuk::SceneManager::GetInstance().GetActiveScene()->GetAllObjects();
	std::vector<breuk::GameObject*> pCollisionObjects{};
	for (breuk::GameObject* pObject : pObjects)
	{
		breuk::RectCollsision* pRectComp = pObject->GetComponentOfType<breuk::RectCollsision>();
		if (pRectComp && (pObject != m_pParent) && pObject->GetComponentOfType<breuk::PhysicsComponent>()->IsStatic())
			pCollisionObjects.push_back(pObject);
	}

	//Final stuff
	glm::vec2 finalCollisionDirX{};
	glm::vec2 finalCollisionDirY{};
	glm::vec2 movementVelocityX{ m_MovementDistance.x * deltaT + m_MovementVelocity.x, 0 };
	glm::vec2 movementVelocityY{ 0, m_MovementDistance.y * deltaT + m_MovementVelocity.y };

	glm::vec2 pos = m_pParent->GetTransform()->GetPosition();
	SDL_Rect collisionRect = pRectColl->GetRect();
	SDL_Rect objectRect = { (int)pos.x + collisionRect.x, (int)pos.y + collisionRect.y, collisionRect.w, collisionRect.h };
	for (breuk::GameObject* pObject : pCollisionObjects)
	{
		breuk::RectCollsision* pRectComp = pObject->GetComponentOfType<breuk::RectCollsision>();
		glm::vec2 posOther = pObject->GetTransform()->GetPosition();
		collisionRect = pRectComp->GetRect();
		SDL_Rect otherRect = SDL_Rect{ (int)posOther.x + collisionRect.x, (int)posOther.y + collisionRect.y, collisionRect.w, collisionRect.h };

		glm::vec2 collisionDirX{};
		glm::vec2 collisionDirY{};

		float timeX = pRectColl->SweptAABB(objectRect, otherRect, collisionDirX, movementVelocityX);
		float timeY = pRectColl->SweptAABB(objectRect, otherRect, collisionDirY, movementVelocityY);

		if (1.f > timeX)
		{
			m_LeftRightTime = timeX;
			finalCollisionDirX = collisionDirX;
		}
		if (1.f > timeY)
		{
			m_UpDownTime = timeY;
			finalCollisionDirY = collisionDirY;
		}
	}
	DecideBlock(finalCollisionDirX, finalCollisionDirY);

	glm::vec2 temp{};
	temp.x = movementVelocityX.x * m_LeftRightTime;
	temp.y = movementVelocityY.y * m_UpDownTime;
	m_pParent->GetTransform()->SetPosition(pos.x + temp.x, pos.y + temp.y);

	if(pos.y + temp.y >= breuk::Settings::m_WindowHeight)
		breuk::ObserverManager::GetInstance().Notify(m_pParent, breuk::Event::FallThroughEvent);
}

void breuk::PhysicsComponent::DecideBlock(const glm::vec2& blockNormDirX, const glm::vec2& blockNormDirY)
{
	//Coll up
	if ((blockNormDirY.y > -FLT_EPSILON) && (m_MovementVelocity.y < FLT_EPSILON))
	{
		m_UpDownTime = 1.f;
	}
	//Coll down
	if ((blockNormDirY.y < -FLT_EPSILON) && (m_MovementVelocity.y > FLT_EPSILON))
	{
		m_HasCollDown = true;

		breuk::ObserverManager::GetInstance().Notify(m_pParent, breuk::Event::LandingEvent);
	}	
	//Coll left
	if ((blockNormDirX.x > FLT_EPSILON) && (m_MovementVelocity.x < -FLT_EPSILON))
	{
		m_HasCollLeft = true;
	}
	//Coll right
	if ((blockNormDirX.x < -FLT_EPSILON) && (m_MovementVelocity.x > FLT_EPSILON))
	{
		m_HasCollRight = true;
	}
}

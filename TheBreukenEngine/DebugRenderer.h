#pragma once
#include "Singleton.h"

namespace breuk
{
	class DebugRenderer final : public Singleton<DebugRenderer>
	{
	public:
		void DrawEmptyRect(const SDL_Rect& rect, const SDL_Color& color) const;
		void DrawLine(const glm::vec3& pos1, const glm::vec3& pos2, const SDL_Color& color) const;
		void DrawDirection(const glm::vec3& start, const glm::vec3& direction, float length, const SDL_Color& color) const;

	private:

	};
}
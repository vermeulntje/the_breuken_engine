#pragma once
#include "Singleton.h"
#include <fstream>

namespace breuk
{
	class GameObject;
	class Font;

	class ResourceManager final : public breuk::Singleton<breuk::ResourceManager>
	{
	public:
		void Init(const std::string& data);

		void OpenIFStream(const std::string& fileName, std::ifstream& stream) const;
		SDL_Texture* LoadTexture(const std::string& file) const;
		breuk::Font* LoadFont(const std::string& file, unsigned int size, breuk::GameObject* pParent = nullptr) const;

	private:
		friend class breuk::Singleton<breuk::ResourceManager>;
		ResourceManager() = default;

		std::string m_DataPath;
	};
}

#include "TheBreukenEnginePCH.h"
#include "Logger.h"

breuk::Logger::~Logger() 
{

}

void breuk::Logger::Log(const std::string& message, const MessageLevel level) 
{
	std::string fullMessage;

	switch (level)
	{
	case MessageLevel::Message:
		fullMessage = "[Messsage] : " + message;
		break;	
	case MessageLevel::Log:
		fullMessage = "[Log] : " + message;
		break;	
	case MessageLevel::Warning:
		fullMessage = "[Warning] : " + message;
		break;
	case MessageLevel::Error:
		fullMessage = "[Messsage] : " + message;
		break;
	}

	std::cout << fullMessage << '\n';
}

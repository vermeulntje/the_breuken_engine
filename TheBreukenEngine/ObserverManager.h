#pragma once
#include "Singleton.h"
#include "Observer.h"

#include <vector>

namespace breuk
{

	class Observer;
	class GameObject;

	class ObserverManager final : public Singleton<ObserverManager>
	{
	public:
		ObserverManager();
		~ObserverManager();

		ObserverManager(ObserverManager& other) = delete;
		ObserverManager(ObserverManager&& other) = delete;
		ObserverManager operator=(ObserverManager& other) = delete;
		ObserverManager& operator=(ObserverManager&& other) = delete;

		void Notify(breuk::GameObject* pObject, const breuk::Event event);

		void AddObserver(breuk::Observer* pObserver);
		void RemoveObserver(breuk::Observer* pObserver);

	private:
		//Keyboard
		std::vector<Observer*> m_Observers;
	};
}

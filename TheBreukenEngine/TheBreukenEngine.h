#pragma once
struct SDL_Window;

namespace breuk
{
	class TheBreukenEngine
	{
	public:
		TheBreukenEngine() = default;
		virtual ~TheBreukenEngine() = default;

		TheBreukenEngine(TheBreukenEngine& other) = delete;
		TheBreukenEngine(TheBreukenEngine&& other) = delete;
		TheBreukenEngine& operator=(TheBreukenEngine& other) = delete;
		TheBreukenEngine& operator=(TheBreukenEngine&& other) = delete;

		virtual void Initialize();
		virtual void Cleanup();

		virtual void LoadGame() const = 0;
		virtual void Run() = 0;

	protected:
		SDL_Window* m_Window{};
	};
}
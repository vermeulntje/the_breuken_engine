#pragma once
#include "Singleton.h"

#include <vector>

namespace breuk
{
	class SoundManager final : public Singleton<SoundManager>
	{
	public:
		SoundManager();
		~SoundManager();

		SoundManager(SoundManager& other) = delete;
		SoundManager(SoundManager&& other) = delete;
		SoundManager operator=(SoundManager& other) = delete;
		SoundManager& operator=(SoundManager&& other) = delete;

		void PlaySoundLooped(const std::string& file);
	};
}

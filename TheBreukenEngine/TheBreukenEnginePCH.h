#pragma once

#include <stdio.h>
#include <iostream> // std::cout
#include <sstream> // stringstream
#include <vector>

#pragma warning(push)
#pragma warning(disable : 26812)
#pragma warning(disable : 26495)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#include <glm/geometric.hpp>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable : 26812)
#pragma warning(disable : 26495)
#pragma warning(disable : 26812)
#pragma warning(disable : 26451)
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#pragma warning(pop)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#define SAFE_DELETE(p) if ((p) != nullptr) { delete (p); (p) = nullptr; }
#define SAFE_DELETE_ARRAY(p) if ((p) != nullptr) { delete[] (p); (p) = nullptr; }

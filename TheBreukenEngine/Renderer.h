#pragma once
#include "Singleton.h"

struct SDL_Window;
struct SDL_Renderer;

namespace breuk
{
	class Texture2D;

	class Renderer final : public Singleton<Renderer>
	{
	public:
		void Init(SDL_Window* window);
		void Render() const;
		void Destroy();

		//void RenderTexture(const Texture2D& texture, const float x, const float y) const;
		void RenderTexture(const Texture2D& texture, const SDL_Rect& source, const SDL_Rect& dest) const;
		void RenderTexture(const Texture2D& texture, const SDL_Rect& dest) const;

		SDL_Renderer* GetSDLRenderer() const { return m_Renderer; }
	private:
		SDL_Renderer* m_Renderer{};
	};
}


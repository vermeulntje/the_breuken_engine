#pragma once
#include "BaseComponent.h"
#include "Components.h"

struct _TTF_Font;

namespace breuk
{
	class GameObject;

	/**
	 * Simple RAII wrapper for an _TTF_Font
	 */
	class Font final : public breuk::BaseComponent
	{
	public:
		explicit Font(const std::string& fullPath, unsigned int size);
		virtual ~Font();

		Font(const Font&) = delete;
		Font(Font&&) = delete;
		Font& operator= (const Font&) = delete;
		Font& operator= (const Font&&) = delete;

		virtual bool Initialize() override;
		virtual void Draw() const override;
		virtual void Update(const float deltaT) override;

		inline void SetText(const std::string& text)
		{
			m_Text = text;
			m_NeedsUpdate = true;
		}
		inline void SetColor(const SDL_Color& color)
		{ 
			m_Color = color; 
		}
		inline void SetSize(const unsigned int size) 
		{
			m_Size = size;
			m_pFont = TTF_OpenFont(m_Path.c_str(), m_Size);
			if (m_pFont == nullptr)
				throw std::runtime_error(std::string("Failed to load font: ") + SDL_GetError());
		}
		inline void SetSourceRect(const float x, const float y, const float w, const float h)
		{
			m_SrcTopFrac = x;
			m_SrcLeftFrac = y;
			m_SrcWidthFrac = w;
			m_SrcHeightFrac = h;
		}
		inline void SetLocalTransform(const glm::vec2& transform) 
		{
			m_pLocalTransform->SetPosition(transform.x, transform.y);
		}
		inline _TTF_Font* GetFont() const 
		{
			return m_pFont;
		}
		inline breuk::Transform* GetLocalTransform() const 
		{
			return m_pLocalTransform;
		}
		inline breuk::Texture2D* GetTexture2D() const 
		{
			return m_pTexture;
		}


	private:
		//SDL
		_TTF_Font* m_pFont;

		//Components
		breuk::Texture2D* m_pTexture;
		breuk::Transform* m_pLocalTransform;

		//Settings
		float m_SrcTopFrac;
		float m_SrcLeftFrac;
		float m_SrcWidthFrac;
		float m_SrcHeightFrac;
		std::string m_Text;
		std::string m_Path;
		SDL_Color m_Color;
		unsigned int m_Size;

		//Core
		bool m_NeedsUpdate;
	};
}

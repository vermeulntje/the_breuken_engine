#include "TheBreukenEnginePCH.h"
#include "MovementState.h"
#include "Components.h"
#include "GameObject.h"

breuk::MovementState::MovementState()
	:BaseComponent()
	, m_ActiveSprite(nullptr)
	, m_RightWalkSprite(nullptr)
	, m_LeftWalkSprite(nullptr)
	, m_AttackLeftSprite(nullptr)
	, m_AttackRightSprite(nullptr)
	, m_CaptureSprite(nullptr)
	, m_DeathSprite(nullptr)
	, m_MovementState(breuk::Movement::LookingRight)
	, m_JumpState(breuk::Jumping::OnGround)
	, m_LivingState(breuk::Living::Alive)
{
}
breuk::MovementState::~MovementState() 
{
}

bool breuk::MovementState::Initialize()
{
	m_ActiveSprite = m_RightWalkSprite;
	m_Initialised = true;
	return m_Initialised;
}
void breuk::MovementState::Draw() const
{
	m_ActiveSprite->Draw();
}

void breuk::MovementState::GotCaptured() 
{
	if (m_LivingState == breuk::Living::Alive)
	{
		m_LivingState = breuk::Living::Captured;
		m_ActiveSprite = m_CaptureSprite;
	}
}
void breuk::MovementState::GotKilled() 
{
	if (m_LivingState != breuk::Living::Dead)
	{
		m_LivingState = breuk::Living::Dead;
		m_ActiveSprite = m_DeathSprite;
	}
}

void breuk::MovementState::StartMovingRight() 
{
	if ((m_MovementState == breuk::Movement::LookingLeft) || (m_MovementState == breuk::Movement::LookingRight))
	{
		m_MovementState = breuk::Movement::WalkingRight;
		m_ActiveSprite = m_RightWalkSprite;
	}
	if (m_MovementState == breuk::Movement::WalkingLeft)
	{
		m_MovementState = breuk::Movement::LookingRight;
		m_ActiveSprite = m_RightWalkSprite;
	}

	m_pParent->GetComponentOfType<breuk::PhysicsComponent>()->AddBaseMovement({ 85.f, 0.f });
}
void breuk::MovementState::StopMovingRight() 
{
	if ((m_MovementState == breuk::Movement::LookingLeft) || (m_MovementState == breuk::Movement::LookingRight))
	{
		m_MovementState = breuk::Movement::WalkingLeft;
		m_ActiveSprite = m_LeftWalkSprite;
	}
	if (m_MovementState == breuk::Movement::WalkingRight)
	{
		m_MovementState = breuk::Movement::LookingRight;
		m_ActiveSprite = m_RightWalkSprite;
	}
	m_pParent->GetComponentOfType<breuk::PhysicsComponent>()->AddBaseMovement({ -85.f, 0.f });
}
void breuk::MovementState::StartMovingLeft() 
{
	if ((m_MovementState == breuk::Movement::LookingLeft) || (m_MovementState == breuk::Movement::LookingRight))
	{
		m_MovementState = breuk::Movement::WalkingLeft;
		m_ActiveSprite = m_LeftWalkSprite;
	}
	if (m_MovementState == breuk::Movement::WalkingRight)
	{
		m_MovementState = breuk::Movement::LookingLeft;
		m_ActiveSprite = m_LeftWalkSprite;
	}
	m_pParent->GetComponentOfType<breuk::PhysicsComponent>()->AddBaseMovement({ -85.f, 0.f });
}
void breuk::MovementState::StopMovingLeft() 
{
	if ((m_MovementState == breuk::Movement::LookingLeft) || (m_MovementState == breuk::Movement::LookingRight))
	{
		m_MovementState = breuk::Movement::WalkingRight;
		m_ActiveSprite = m_RightWalkSprite;
	}
	if (m_MovementState == breuk::Movement::WalkingLeft)
	{
		m_MovementState = breuk::Movement::LookingLeft;
		m_ActiveSprite = m_LeftWalkSprite;
	}
	m_pParent->GetComponentOfType<breuk::PhysicsComponent>()->AddBaseMovement({ 85.f, 0.f });
}

void breuk::MovementState::Jumped() 
{
	if (m_JumpState == breuk::Jumping::OnGround) 
	{
		m_JumpState = breuk::Jumping::InAir;
		m_pParent->GetComponentOfType<breuk::PhysicsComponent>()->AddForce({ 0.f, -330.f });
	}
}
void breuk::MovementState::Landed() 
{
	m_JumpState = breuk::Jumping::OnGround;
}

void breuk::MovementState::Attack()
{
	
}
void breuk::MovementState::EndAttack()
{

}

#include "TheBreukenEnginePCH.h"
#include "TheBreukenEngine.h"
#include "BaseComponent.h"
#include "Components.h"

breuk::BaseComponent::BaseComponent()
	:m_pParent(nullptr)
	, m_Initialised{false}
	, m_UnInitChildComponents{}
	, m_InitChildComponents{}
{
}
breuk::BaseComponent::~BaseComponent()
{
	for (breuk::BaseComponent* pComp : m_UnInitChildComponents)
	{
		delete pComp;
		pComp = nullptr;
	}	
	for (breuk::BaseComponent* pComp : m_InitChildComponents)
	{
		delete pComp;
		pComp = nullptr;
	}
}

bool breuk::BaseComponent::CoreInitialize()
{
	bool coreInitDone{false};
	if (m_pParent)
	{
		InitComponents();
		coreInitDone = true;
	}
	return coreInitDone;
}
void breuk::BaseComponent::Draw() const 
{
	for (size_t idx{}; idx < m_InitChildComponents.size(); idx++)
	{
		m_InitChildComponents[idx]->Draw();
	}
}
void breuk::BaseComponent::LateDraw() const 
{
	for (size_t idx{}; idx < m_InitChildComponents.size(); idx++)
	{
		m_InitChildComponents[idx]->LateDraw();
	}
}
void breuk::BaseComponent::CoreUpdate(const float deltaT) 
{
	UNREFERENCED_PARAMETER(deltaT);
	InitComponents();
	for (size_t idx{}; idx < m_InitChildComponents.size(); idx++)
	{
		m_InitChildComponents[idx]->CoreUpdate(deltaT);
	}
}
void breuk::BaseComponent::Update(const float deltaT) 
{
	UNREFERENCED_PARAMETER(deltaT);
	for (size_t idx{}; idx < m_InitChildComponents.size(); idx++)
	{
		m_InitChildComponents[idx]->Update(deltaT);
	}
}
void breuk::BaseComponent::LateUpdate(const float deltaT) 
{
	UNREFERENCED_PARAMETER(deltaT);
	for (size_t idx{}; idx < m_InitChildComponents.size(); idx++)
	{
		m_InitChildComponents[idx]->LateUpdate(deltaT);
	}
}

void breuk::BaseComponent::InitComponents()
{
	//Try and initialize new components
	for (size_t idx{}; idx < m_UnInitChildComponents.size(); idx++)
	{
		if (m_UnInitChildComponents[idx]->Initialize())
		{
			m_InitChildComponents.push_back(m_UnInitChildComponents[idx]);
			m_UnInitChildComponents.erase(m_UnInitChildComponents.begin() + idx);
			idx--;
		}
	}
}

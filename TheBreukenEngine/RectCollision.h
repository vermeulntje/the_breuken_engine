#pragma once
#include "BaseComponent.h"

namespace breuk
{
	class LineNormCollision;

	class RectCollsision final : public BaseComponent
	{
	public:
		RectCollsision();
		virtual ~RectCollsision();

		RectCollsision(const RectCollsision&) = delete;
		RectCollsision(RectCollsision&&) = delete;
		RectCollsision& operator= (const RectCollsision&) = delete;
		RectCollsision& operator= (const RectCollsision&&) = delete;

		virtual bool Initialize() override;
		virtual void LateDraw() const override;

		SDL_Rect GetSweptBroadphaseRect(const SDL_Rect& object, const glm::vec2& movementVelocity);
		bool IsColliding(const SDL_Rect& src, const SDL_Rect& dest);
		float SweptAABB(const SDL_Rect& object, const SDL_Rect& rect, glm::vec2& collisionDir, const glm::vec2& movementVelocity);

		inline void SetRect(const SDL_Rect& rect) 
		{
			m_Rect = rect;
		}
		inline SDL_Rect GetRect() const 
		{
			return m_Rect;
		}
		inline void SetColor(const SDL_Color& color)
		{
			m_Color = color;
		}

	private:
		SDL_Rect m_Rect;
		SDL_Color m_Color;
	};
}
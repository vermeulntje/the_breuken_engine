#include "TheBreukenEnginePCH.h"
#include "InputManager.h"
#include "BaseCommand.h"
#include "Logger.h"

breuk::InputManager::InputManager()
	:m_UMapCommands{}
	, m_UMapControllerCommands{}
	//Set the max amount of input devices
	//Max is set by xinput
	, m_CurrentState(XUSER_MAX_COUNT, XINPUT_STATE{})
	, m_ControllerDetected(XUSER_MAX_COUNT, false)
	, m_AnyControllerDetected(false)
{
	//Set the max amount of commands based on all the inputs possible
	//in SDL, not all will be use!
	//SDL_SCANCODE_AUDIOFASTFORWARD == 286, last element in the enum
	for (int idx{}; idx <= int(SDL_SCANCODE_AUDIOFASTFORWARD); idx++)
	{
		m_UMapCommands.insert({idx, nullptr});
	}
}
breuk::InputManager::~InputManager() 
{
	auto pCommand = m_UMapCommands.begin();
	while (true) 
	{
		SAFE_DELETE(pCommand->second);
		pCommand++;
		if (pCommand == m_UMapCommands.end())
			break;
	}	
}

bool breuk::InputManager::ProcessInput()
{
	m_ControllerDetected = std::vector<bool>(XUSER_MAX_COUNT, false);
	m_AnyControllerDetected = false;

	DWORD dwResult;
	for (DWORD i = 0; i < XUSER_MAX_COUNT; i++)
	{
		// Simply get the state of the controller from XInput.
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));
		XInputGetState(0, &state);
		dwResult = XInputGetState(i, &state);

		//Check for succes
		if (dwResult == ERROR_SUCCESS)
		{
			// Controller is connected 
			m_CurrentState[i] = state;
			m_ControllerDetected[i] = true;
			m_AnyControllerDetected = true;
			WORD gamePad = m_CurrentState[i].Gamepad.wButtons;
			
			int nrButtons{ 15 };
			for (int shift{}; shift < nrButtons; shift++)
			{
				unsigned int shiftf = (0b0000000000000001 << shift);
			
				if (gamePad & shiftf)
				{
					if (m_UMapControllerCommands[shiftf])
						m_UMapControllerCommands[shiftf]->SetKeyState(true);
				}
				else
				{
					if (m_UMapControllerCommands[shiftf])
						m_UMapControllerCommands[shiftf]->SetKeyState(false);
				}
			}
		}
		else
		{
			// Controller is not connected 
		}
	}

	SDL_Event e{};
	while (SDL_PollEvent(&e)) 
	{
		if (e.type == SDL_QUIT) 
		{
			return false;
		}

		if (m_AnyControllerDetected == false)
		{
			if (e.type == SDL_KEYUP)
			{
				if (e.key.keysym.scancode <= int(SDL_SCANCODE_AUDIOFASTFORWARD))
					if (m_UMapCommands[e.key.keysym.scancode])
						m_UMapCommands[e.key.keysym.scancode]->SetKeyState(false);
			}
			if (e.type == SDL_KEYDOWN)
			{
				if (e.key.keysym.scancode <= int(SDL_SCANCODE_AUDIOFASTFORWARD))
					if (m_UMapCommands[e.key.keysym.scancode])
						m_UMapCommands[e.key.keysym.scancode]->SetKeyState(true);
			}
		}

	}

	//Returning true just prevents shutdown
	return true;
}

void breuk::InputManager::HandleInput() 
{
	int nrButtons{ 15 };
	for (int shift{}; shift < nrButtons; shift++)
	{ 
		unsigned int shiftf = (0b0000000000000001 << shift);

		BaseCommand* pCom{ m_UMapControllerCommands[(const int)shiftf] };
		if(pCom)
			pCom->Execute();
	}
	if (m_AnyControllerDetected == false)
	{
		for (size_t key{}; key <= int(SDL_SCANCODE_AUDIOFASTFORWARD); key++)
		{
			BaseCommand* pCom{ m_UMapCommands[(const int)key] };
			if (pCom)
				pCom->Execute();
		}
	}
}

void breuk::InputManager::AddCommand(const int newKey, breuk::BaseCommand* pCommand, const int conrollerInput)
{
	//Keyboard
	BaseCommand* pNewCom{ m_UMapCommands[newKey] };
	if (pNewCom)
	{
		breuk::Logger::GetInstance().Log("A command was overwritten! For keyID " + std::to_string(newKey) + '.', breuk::MessageLevel::Log);
		SAFE_DELETE(pNewCom);
		m_UMapControllerCommands[conrollerInput] = nullptr;
	}
	m_UMapCommands[newKey] = pCommand;	
	//Controller
	if (conrollerInput != 0)
	{
		pNewCom = m_UMapControllerCommands[conrollerInput];
		m_UMapControllerCommands[conrollerInput] = pCommand;
	}
}

bool breuk::InputManager::SwapCommand(const int newKey, const int prevKey)
{
	BaseCommand* pNewCom{ m_UMapCommands[newKey] };
	BaseCommand* pPrevCom{ m_UMapCommands[prevKey] };

	if (!pNewCom)
	{
		//The new spot is not occupied with other key
		//So setting succeeds
		m_UMapCommands[prevKey] = nullptr;
		m_UMapCommands[newKey] = pPrevCom;
		return true;
	}
	//Setting didn't succeed
	return false;
}

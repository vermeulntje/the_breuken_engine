#include "TheBreukenEnginePCH.h"
#include "LevelParser.h"
#include "GameObject.h"
#include "ResourceManager.h"

LevelParser::LevelParser()
	:breuk::BaseComponent()
	, m_LevelFile{}
	, m_EnemyFile{}
{
}
LevelParser::~LevelParser()
{
	if (m_LevelFile.is_open())
		m_LevelFile.close();
	if (m_EnemyFile.is_open())
		m_EnemyFile.close();
}

bool LevelParser::Initialize()
{
	m_Initialised = true;
	return m_Initialised;
}

bool LevelParser::OpenLevelFile(const std::string& fileLoc) 
{
	//If by any chance the previous one was still open close it
	if (m_LevelFile.is_open())
		m_LevelFile.close();
	breuk::ResourceManager::GetInstance().OpenIFStream(fileLoc, m_LevelFile);
	if (m_LevelFile.is_open())
		return true;
	return false;
}
bool LevelParser::OpenEnemyFile(const std::string& fileLoc) 
{
	//If by any chance the previous one was still open close it
	if (m_EnemyFile.is_open())
		m_EnemyFile.close();
	breuk::ResourceManager::GetInstance().OpenIFStream(fileLoc, m_EnemyFile);
	if (m_EnemyFile.is_open())
		return true;
	return false;
}

bool LevelParser::ParseLevels(std::vector<char>& container)
{
	if (!m_LevelFile.is_open())
		return false;

	char temp{};

	size_t idx{};
	while (true)
	{
		m_LevelFile.read(&temp, 1);
		container[idx] = temp;
		idx++;
		if (idx >= container.size())
			break;
	}

	m_LevelFile.close();
	return true;
}
bool LevelParser::ParseEnemies(std::vector<std::vector<char>>& container, const int enemyDataSize)
{
	if (!m_EnemyFile.is_open())
		return false;

	//Index to track the number 
	int idx{};
	int leftOverDataSize = enemyDataSize - 1;

	bool zeroFound{false};
	//true to kickstart the loop
	while (true)
	{
		char testData{}; //1 byte is read to check for 0
		char* pFillData{ new char[leftOverDataSize] };

		//Kickstart
		m_EnemyFile.read(&testData, 1);
		if (testData) //Reset the end of file check
			zeroFound = false;
		while (testData != 0)
		{
			//Read in data
			m_EnemyFile.read(pFillData, leftOverDataSize);

			int containerSize = (int)container[idx].size();
			//Resize to prevent uneeded pushbacks
			container[idx].resize(size_t(containerSize + enemyDataSize));
			//Add the data
			container[idx][containerSize] = testData;
			for (int i{}; i < leftOverDataSize; i++) 
			{
				int j = containerSize + 1 + i;
				container[idx][j] = pFillData[i];
			}

			//Restart loop
			m_EnemyFile.read(&testData, 1);
		}

		//Next level
		idx++;
		//Cleanup
		SAFE_DELETE_ARRAY(pFillData);
		//Signaling the end of a level or the end of a file
		if (zeroFound)
			break;
		zeroFound = true;
	}

	m_EnemyFile.close();
	return true;
}

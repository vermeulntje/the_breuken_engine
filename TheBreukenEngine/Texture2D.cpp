#include "TheBreukenEnginePCH.h"
#include "Texture2D.h"
#include "Transform.h"
#include "GameObject.h"
#include "Renderer.h"

breuk::Texture2D::Texture2D(SDL_Texture* texture)
	:breuk::BaseComponent()
	, m_pTexture(texture)
	, m_pLocalTransform{}
	, m_Dest()
	, m_Src()
{
	m_pLocalTransform = AddComponent(new breuk::Transform());
	SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_Dest.w, &m_Dest.h);
	m_Src.w = m_Dest.w;
	m_Src.h = m_Dest.h;
}

breuk::Texture2D::~Texture2D()
{
	SDL_DestroyTexture(m_pTexture);
}

bool breuk::Texture2D::Initialize()
{
	m_Initialised = true;
	return m_Initialised;
}

void breuk::Texture2D::Draw() const
{
	const glm::vec2& pos = m_pParent->GetTransform()->GetPosition();
	const glm::vec2& localPos = m_pLocalTransform->GetPosition();

	SDL_Rect dest{ m_Dest };
	dest.x = int(pos.x + localPos.x);
	dest.y = int(pos.y + localPos.y);
	SDL_Rect src{m_Src};
	breuk::Renderer::GetInstance().RenderTexture(*this, src, dest);
}

breuk::Transform* breuk::Texture2D::GetLocalTransform() const
{
	return m_pLocalTransform;
}
void breuk::Texture2D::SetLocalTransform(const glm::vec2& transform)
{
	m_pLocalTransform->SetPosition(transform.x, transform.y);
}

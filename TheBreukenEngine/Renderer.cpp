#include "TheBreukenEnginePCH.h"
#include "Renderer.h"
#include "SceneManager.h"
#include "Texture2D.h"

void breuk::Renderer::Init(SDL_Window * window)
{
	m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);// | SDL_RENDERER_PRESENTVSYNC);
	if (m_Renderer == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateRenderer Error: ") + SDL_GetError());
	}
}

void breuk::Renderer::Render() const
{
	SDL_SetRenderDrawColor(m_Renderer, 0,0,0,1);

	SDL_RenderClear(m_Renderer);

	SceneManager::GetInstance().Render();
	
	SDL_RenderPresent(m_Renderer);
}

void breuk::Renderer::Destroy()
{
	if (m_Renderer != nullptr)
	{
		SDL_DestroyRenderer(m_Renderer);
		m_Renderer = nullptr;
	}
}

void breuk::Renderer::RenderTexture(const breuk::Texture2D& texture, const SDL_Rect& source, const SDL_Rect& dest) const
{
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), &source, &dest);
}

void breuk::Renderer::RenderTexture(const breuk::Texture2D& texture, const SDL_Rect& dest) const
{
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dest);
}

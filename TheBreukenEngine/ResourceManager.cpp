#include "TheBreukenEnginePCH.h"
#include "ResourceManager.h"
#include <fstream>

#include "Renderer.h"
//Helper header, includes all components
#include "Components.h"

void breuk::ResourceManager::Init(const std::string& dataPath)
{
	m_DataPath = dataPath;

	// load support for png and jpg, this takes a while!

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) 
	{
		throw std::runtime_error(std::string("Failed to load support for png's: ") + SDL_GetError());
	}

	if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG) 
	{
		throw std::runtime_error(std::string("Failed to load support for jpg's: ") + SDL_GetError());
	}

	if (TTF_Init() != 0) 
	{
		throw std::runtime_error(std::string("Failed to load support for fonts: ") + SDL_GetError());
	}
}

void breuk::ResourceManager::OpenIFStream(const std::string& fileName, std::ifstream& stream) const
{
	std::string fullPath{ m_DataPath + fileName };
	stream.open(fullPath, std::ifstream::binary);
}

SDL_Texture* breuk::ResourceManager::LoadTexture(const std::string& file) const
{
	const std::string fullPath = m_DataPath + file;
	SDL_Texture* texture = IMG_LoadTexture(breuk::Renderer::GetInstance().GetSDLRenderer(), fullPath.c_str());
	if (texture == nullptr) 
	{
		throw std::runtime_error(std::string("Failed to load texture: ") + SDL_GetError());
	}
	return texture;
}

breuk::Font* breuk::ResourceManager::LoadFont(const std::string& file, unsigned int size, breuk::GameObject* pParent) const
{
	breuk::Font* pFont = new breuk::Font(m_DataPath + file, size);
	pFont->SetParent(pParent);
	return pFont;
}

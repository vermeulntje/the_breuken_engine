#include "TheBreukenEnginePCH.h"
#include "Sprite.h"
#include "Components.h"

breuk::Sprite::Sprite() 
	:BaseComponent()
	, m_pTexture(nullptr)
	, m_NrFrames(0)
	, m_CurrentFrame(0)
	, m_FrameTime(0)
	, m_CurrentFrameTime(0)
	, m_NrFrameColls(0)
	, m_NrFrameRows(0)
	, m_FrameRect({0,0,0,0})
{
}
breuk::Sprite::~Sprite() 
{
}

bool breuk::Sprite::Initialize() 
{
	m_pTexture->SetSourceRect(0, 0, m_FrameRect.w, m_FrameRect.h);
	m_pTexture->SetDestRect(m_FrameRect.w * 2, m_FrameRect.h * 2);
	m_Initialised = true;
	return m_Initialised;
}
void breuk::Sprite::Update(const float elapsedSec) 
{
	m_CurrentFrameTime += elapsedSec;
	if (m_FrameTime <= m_CurrentFrameTime)
	{
		m_CurrentFrameTime -= m_FrameTime;
		m_CurrentFrame++;

		if (m_CurrentFrame == m_NrFrames)
			m_CurrentFrame = 0;

		int wOffsetIdx = m_CurrentFrame % m_NrFrameColls;
		int hOffsetIdx = m_CurrentFrame / m_NrFrameColls;

		m_pTexture->SetSourceRect(m_FrameRect.x + m_FrameRect.w * wOffsetIdx, m_FrameRect.y + m_FrameRect.h * hOffsetIdx, m_FrameRect.w, m_FrameRect.h);
	}
}

#include "TheBreukenEnginePCH.h"
#include "SceneManager.h"
#include "Scene.h"

breuk::SceneManager::~SceneManager() 
{
	ClearScenes();
}

void breuk::SceneManager::Initialize()
{
	InitScenes();
}
void breuk::SceneManager::Update(const float elapsedSec)
{
	InitScenes();

	if (m_ActiveScene != -1)
	{
		m_InitScenes[m_ActiveScene]->Update(elapsedSec);
	}
}
void breuk::SceneManager::Render()
{
	if (m_ActiveScene != -1)
	{
		m_InitScenes[m_ActiveScene]->Render();
	}
}

breuk::Scene& breuk::SceneManager::CreateScene(const std::string& name)
{
	breuk::Scene* scene = new breuk::Scene(name);
	m_UnInitScenes.push_back(scene);
	if (m_ActiveScene == -1)
		m_ActiveScene = 0;
	return *scene;
}
void breuk::SceneManager::InitScenes()
{
	//Try and initialize new components
	for (size_t idx{}; idx < m_UnInitScenes.size(); idx++)
	{
		if (m_UnInitScenes[idx])
			if (m_UnInitScenes[idx]->Initialize())
			{
				m_InitScenes.push_back(m_UnInitScenes[idx]);
				m_UnInitScenes.erase(m_UnInitScenes.begin() + idx);
				idx--;
			}
	}
}

void breuk::SceneManager::ClearScenes()
{
	for (size_t idx{}; idx < m_UnInitScenes.size(); idx++)
		SAFE_DELETE(m_UnInitScenes[idx]);
	m_UnInitScenes.clear();
	for (size_t idx{}; idx < m_InitScenes.size(); idx++)
		SAFE_DELETE(m_InitScenes[idx]);
	m_InitScenes.clear();

	m_ActiveScene = -1;
}
void breuk::SceneManager::ClearActiveScene()
{
	SAFE_DELETE(m_InitScenes[m_ActiveScene]);
	m_InitScenes.erase(m_InitScenes.begin() + m_ActiveScene);
	m_ActiveScene = -1;
}

void breuk::SceneManager::SetActiveScene(const int idx)
{
	int nrInitScenes = (int)m_InitScenes.size();
	int activeIdx = (idx < nrInitScenes) ? idx : nrInitScenes;
	m_ActiveScene = activeIdx;
}
void breuk::SceneManager::SetActiveScene(const std::string& name)
{
	for (size_t idx{}; idx < m_InitScenes.size(); idx++)
	{
		if (m_InitScenes[idx]->GetName() == name) 
		{
			m_ActiveScene = (int)idx;
			break;
		}
	}
}

breuk::Scene* breuk::SceneManager::GetActiveScene() const
{
	if (m_ActiveScene != -1)
		return m_InitScenes[m_ActiveScene];
	return nullptr;
}
breuk::Scene* breuk::SceneManager::GetScene(const std::string& name) const
{
	for (size_t idx{}; idx < m_InitScenes.size(); idx++)
	{
		if (m_InitScenes[idx]->GetName() == name)
			return m_InitScenes[idx];
	}
	return nullptr;
}

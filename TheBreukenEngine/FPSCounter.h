#pragma once
#include "BaseComponent.h"

namespace breuk
{
	class GameObject;

	class FPSCounter final : public breuk::BaseComponent
	{
	public:
		FPSCounter();
		virtual ~FPSCounter();

		FPSCounter(FPSCounter& other) = delete;
		FPSCounter(FPSCounter&& other) = delete;
		FPSCounter& operator=(FPSCounter& other) = delete;
		FPSCounter& operator=(FPSCounter&& other) = delete;

		virtual bool Initialize() override;
		virtual void Update(const float deltaT) override;

		bool CheckTime(int& count);

	private:
		int m_Count;
		float m_ElapsedTime;
	};
}

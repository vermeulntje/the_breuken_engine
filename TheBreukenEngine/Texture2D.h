#pragma once
#include "BaseComponent.h"

struct SDL_Texture;

namespace breuk
{
	class GameObject;
	class Transform;

	class Texture2D final : public breuk::BaseComponent
	{
	public:
		explicit Texture2D(SDL_Texture* texture);
		virtual ~Texture2D();

		Texture2D(const Texture2D&) = delete;
		Texture2D(Texture2D&&) = delete;
		Texture2D operator= (const Texture2D&) = delete;
		Texture2D& operator= (const Texture2D&&) = delete;

		virtual bool Initialize() override;
		virtual void Draw() const override;

		inline SDL_Texture* GetSDLTexture() const 
		{
			return m_pTexture;
		}
		inline void SetSDLTexture(SDL_Texture* pText) 
		{
			m_pTexture = pText;
		}
		inline void SetDestRect(const int w, const int h)
		{
			m_Dest.w = w;
			m_Dest.h = h;
		}
		inline void SetSourceRect(const int x, const int y, const int w, const int h)
		{
			m_Src.x = x;
			m_Src.y = y;
			m_Src.w = w;
			m_Src.h = h;
		}

		inline void SetDestFullSize() 
		{ 
			SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_Dest.w, &m_Dest.h);
		}
		inline void SetSrcFullSize()
		{
			SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_Src.w, &m_Src.h);
		}


		breuk::Transform* GetLocalTransform() const;
		void SetLocalTransform(const glm::vec2& transform);

	private:
		//SDL
		SDL_Texture* m_pTexture;

		//Settings
		SDL_Rect m_Dest;
		SDL_Rect m_Src;

		//Components
		breuk::Transform* m_pLocalTransform;
	};
}

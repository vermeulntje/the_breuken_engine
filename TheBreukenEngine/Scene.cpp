#include "TheBreukenEnginePCH.h"
#include "Scene.h"
#include "GameObject.h"

unsigned int breuk::Scene::m_IdCounter = 0;

breuk::Scene::Scene(const std::string& name) 
	:m_Name(name) 
	, m_UnInitObjects{}
	, m_InitObjects{}
{
}
breuk::Scene::~Scene() 
{
	for (GameObject* pObject : m_UnInitObjects)
	{
		SAFE_DELETE(pObject);
	}	
	for (GameObject* pObject : m_InitObjects)
	{
		SAFE_DELETE(pObject);
	}
}

void breuk::Scene::Add(GameObject* object)
{
	m_UnInitObjects.push_back(object);
}

// CORE
//*****
bool breuk::Scene::Initialize()
{
	InitObjects();
	return true;
}
void breuk::Scene::Update(const float elapsedSec)
{
	//Initialize new objects
	InitObjects();
	//Update already initialized objects
	size_t size = m_InitObjects.size();
	for (size_t idx{}; idx < size; idx++)
	{
		m_InitObjects[idx]->CoreUpdate(elapsedSec);
	}
	for (size_t idx{}; idx < size; idx++)
	{
		m_InitObjects[idx]->Update(elapsedSec);
	}
	for (size_t idx{}; idx < size; idx++)
	{
		m_InitObjects[idx]->LateUpdate(elapsedSec);
	}
}
void breuk::Scene::Render() const
{
	size_t size = m_InitObjects.size();

	for (size_t idx{}; idx < size; idx++)
	{
		m_InitObjects[idx]->Render();
	}
	for (size_t idx{}; idx < size; idx++)
	{
		m_InitObjects[idx]->LateRender();
	}
}

void breuk::Scene::InitObjects()
{
	for (size_t idx{}; idx < m_UnInitObjects.size(); idx++)
	{
		if (m_UnInitObjects[idx]->Initialize())
		{
			m_InitObjects.push_back(m_UnInitObjects[idx]);
			m_UnInitObjects.erase(m_UnInitObjects.begin() + idx);
			idx--;
		}
	}
}

//************
// TAG GETTERS
breuk::GameObject* breuk::Scene::GetObjectWithTag(const std::string& tag)
{
	for (breuk::GameObject* object : m_InitObjects)
	{
		if (object->GetTag() == tag)
			return object;
	}
	return nullptr;
}
breuk::GameObject* breuk::Scene::GetObjectWithTag(const std::string& tag) const
{
	for (breuk::GameObject* object : m_InitObjects)
	{
		if (object->GetTag() == tag)
			return object;
	}
	return nullptr;
}
std::vector<breuk::GameObject*> breuk::Scene::GetAllObjectsWithTag(const std::string& tag)
{
	std::vector<breuk::GameObject*> temp{};
	for (breuk::GameObject* object : m_InitObjects)
	{
		if (object->GetTag() == tag)
			temp.push_back(object);
	}
	return temp;
}
std::vector<breuk::GameObject*> breuk::Scene::GetAllObjectsWithTag(const std::string& tag) const
{
	std::vector<breuk::GameObject*> temp{};
	for (breuk::GameObject* object : m_InitObjects)
	{
		if (object->GetTag() == tag)
			temp.push_back(object);
	}
	return temp;
}

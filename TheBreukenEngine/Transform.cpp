#include "TheBreukenEnginePCH.h"
#include "Transform.h"
#include "GameObject.h"

breuk::Transform::Transform()
	:breuk::BaseComponent()
	, m_Position{}
{
}
breuk::Transform::~Transform()
{

}

void breuk::Transform::SetPosition(const float x, const float y)
{
	m_Position.x = x;
	m_Position.y = y;
}

bool breuk::Transform::Initialize()
{
	m_Initialised = true;
	return m_Initialised;
}

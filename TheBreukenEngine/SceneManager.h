#pragma once
#include "Singleton.h"

namespace breuk
{
	class Scene;

	class SceneManager final : public breuk::Singleton<breuk::SceneManager>
	{
	public:
		~SceneManager();

		SceneManager(SceneManager& other) = delete;
		SceneManager(SceneManager&& other) = delete;
		SceneManager operator=(SceneManager& other) = delete;
		SceneManager& operator=(SceneManager&& other) = delete;

		// CORE
		//*****
		void Initialize();
		void Update(const float elapsedSec);
		void Render();

		void InitScenes();

		breuk::Scene& CreateScene(const std::string& name);
		
		void ClearScenes();
		void ClearActiveScene();

		void SetActiveScene(const int idx);
		void SetActiveScene(const std::string& name);

		breuk::Scene* GetActiveScene() const;
		breuk::Scene* GetScene(const std::string& name) const;

	private:
		friend class breuk::Singleton<breuk::SceneManager>;
		SceneManager() = default;

		std::vector<breuk::Scene*> m_UnInitScenes;
		std::vector<breuk::Scene*> m_InitScenes;
		int m_ActiveScene = -1;
	};
}

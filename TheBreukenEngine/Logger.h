#pragma once
#include "Singleton.h"

namespace breuk
{
	enum MessageLevel
	{
		Message,
		Log,
		Warning,
		Error
	};

	class Logger final : public breuk::Singleton<breuk::Logger>
	{
	public:
		~Logger();

		Logger(Logger& other) = delete;
		Logger(Logger&& other) = delete;
		Logger operator=(Logger& other) = delete;
		Logger& operator=(Logger&& other) = delete;

		void Log(const std::string& messagem, const MessageLevel level);

	private:
		friend class breuk::Singleton<breuk::Logger>;
		Logger() = default;
	};
}

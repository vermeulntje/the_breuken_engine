#pragma once
#include "Transform.h"
#include "Logger.h"

namespace breuk
{
	class BaseComponent;

	class GameObject final
	{
	public:
		explicit GameObject(const std::string& tag = "");
		~GameObject();

		GameObject(const breuk::GameObject& other) = delete;
		GameObject(breuk::GameObject&& other) = delete;
		breuk::GameObject& operator=(const breuk::GameObject& other) = delete;
		breuk::GameObject& operator=(breuk::GameObject&& other) = delete;
		
		// CORE
		//*****
		bool Initialize();
		void CoreUpdate(const float elapsedSec);
		void Update(const float elapsedSec);
		void LateUpdate(const float elapsedSec);
		void Render() const;
		void LateRender() const;

		void InitComponents();

		inline virtual void SetTag(const std::string& tag) { m_Tag = tag; }
		inline virtual std::string GetTag() const { return m_Tag; }

		inline Transform* GetTransform() 
		{
			return m_pTransform;
		}

		//Templates don't like inheritance and classes while in '.cpp's
		//Compiler needs to know about the types it will need to construct
		//	the moment it learns about the function
		template<class T>
		inline T* AddComponent(T* pComponent)
		{
			for (breuk::BaseComponent* component : m_UnInitComponents)
			{
				//Components was already added
				if (component == pComponent)
					return nullptr;
			}		
			for (breuk::BaseComponent* component : m_InitComponents)
			{
				//Components was already added
				if (component == pComponent)
					return nullptr;
			}

			pComponent->SetParent(this);
			m_UnInitComponents.push_back(pComponent);
			return pComponent;
		}		
		template<class T>
		inline void RemoveComponent(T* pComponent)
		{
			for (breuk::BaseComponent* component : m_UnInitComponents)
			{
				//Components was already added
				if (component == pComponent)
				{
					m_UnInitComponents.erase(component);
					return;
				}
			}		
			for (breuk::BaseComponent* component : m_InitComponents)
			{
				//Components was already added
				if (component == pComponent)
				{
					m_InitComponents.erase(component);
					return;
				}
			}
		}
		inline std::vector<BaseComponent*> GetComponents() { return m_InitComponents; }

		//*************
		// TYPE GETTERS
		template<class T>
		inline T* GetComponentOfType()
		{
			for (breuk::BaseComponent* component : m_InitComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					return cast;
			}
			return nullptr;
		}
		template<class T>
		inline T* GetComponentOfType() const
		{
			for (breuk::BaseComponent* component : m_InitComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					return cast;
			}
			return nullptr;
		}
		template<class T>
		inline std::vector<T*> GetAllComponentOfType()
		{
			std::vector<T*> temp{};
			for (breuk::BaseComponent* component : m_InitComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					temp.push_back(temp);
			}
			return temp;
		}
		template<class T>
		inline std::vector<T*> GetAllComponentOfType() const
		{
			std::vector<T*> temp{};
			for (breuk::BaseComponent* component : m_InitComponents)
			{
				T* cast = dynamic_cast<T*>(component);
				if (cast)
					temp.push_back(temp);
			}
			return temp;
		}

	private:
		std::vector<BaseComponent*> m_UnInitComponents;
		std::vector<BaseComponent*> m_InitComponents;

		breuk::Transform* m_pTransform;
		std::string m_Tag;
	};
}

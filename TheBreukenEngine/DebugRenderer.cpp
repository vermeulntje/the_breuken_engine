#include "TheBreukenEnginePCH.h"
#include "DebugRenderer.h"
#include "Renderer.h"
#include "Transform.h"

void breuk::DebugRenderer::DrawEmptyRect(const SDL_Rect& rect, const SDL_Color& color) const
{
	SDL_Renderer* pRenderer = breuk::Renderer::GetInstance().GetSDLRenderer();
	SDL_SetRenderDrawColor(pRenderer, color.r, color.g, color.b, color.a);
	SDL_RenderDrawRect(pRenderer, &rect);

}

void breuk::DebugRenderer::DrawLine(const glm::vec3& pos1, const glm::vec3& pos2, const SDL_Color& color) const
{
	SDL_Renderer* pRenderer = breuk::Renderer::GetInstance().GetSDLRenderer();
	SDL_SetRenderDrawColor(pRenderer, color.r, color.g, color.b, color.a);
	SDL_RenderDrawLine(pRenderer, (int)pos1.x, (int)pos1.y, (int)pos2.x, (int)pos2.y);
}

void breuk::DebugRenderer::DrawDirection(const glm::vec3& pos1, const glm::vec3& direction, float length, const SDL_Color& color) const
{
	SDL_Renderer* pRenderer = breuk::Renderer::GetInstance().GetSDLRenderer();
	SDL_SetRenderDrawColor(pRenderer, color.r, color.g, color.b, color.a);
	glm::vec3 pos2{ pos1 + direction * length };
	SDL_RenderDrawLine(pRenderer, (int)pos1.x, (int)pos1.y, (int)pos2.x, (int)pos2.y);
}
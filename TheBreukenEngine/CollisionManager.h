#pragma once
#include "Singleton.h"
#include <vector>

namespace breuk
{
	class BaseComponent;

	class CollisionManager final : public Singleton<CollisionManager>
	{
	public:
		CollisionManager();
		~CollisionManager();

		CollisionManager(CollisionManager& other) = delete;
		CollisionManager(CollisionManager&& other) = delete;
		CollisionManager operator=(CollisionManager& other) = delete;
		CollisionManager& operator=(CollisionManager&& other) = delete;

		void AddCollisionComp(breuk::BaseComponent* pComponent);
		inline std::vector<breuk::BaseComponent*> GetCollisionComponents() 
		{
			return m_CollisionComponents; 
		}

	private:
		std::vector<breuk::BaseComponent*> m_CollisionComponents;
	};
}

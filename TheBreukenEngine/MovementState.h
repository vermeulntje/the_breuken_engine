#pragma once
#include "BaseComponent.h"

namespace breuk 
{
	enum class Movement
	{
		LookingRight,
		LookingLeft,
		WalkingRight,
		WalkingLeft,
	};
	enum class Jumping
	{
		OnGround,
		InAir,
	};
	enum class Living
	{
		Alive,
		Captured,
		Dead
	};

	class Sprite;

	class MovementState final : public BaseComponent 
	{
	public:
		MovementState();
		virtual ~MovementState();

		MovementState(MovementState& other) = delete;
		MovementState(MovementState&& other) = delete;
		MovementState operator=(MovementState& other) = delete;
		MovementState& operator=(MovementState&& other) = delete;

		virtual bool Initialize() override;
		virtual void Draw() const override;

		// STATES
		//*******
		void GotCaptured();
		void GotKilled();

		void StartMovingRight();
		void StopMovingRight();
		void StartMovingLeft();
		void StopMovingLeft();

		void Jumped();
		void Landed();

		void Attack();
		void EndAttack();

		// TEXTURES
		//*********
		inline void SetRightWalkSprite(Sprite* pRightWalkSprite) 
		{
			m_RightWalkSprite = pRightWalkSprite;
		}	
		inline void SetLeftWalkSprite(Sprite* pLeftWalkSprite)
		{
			m_LeftWalkSprite = pLeftWalkSprite;
		}		
		inline void SetAttackLeftSprite(Sprite* pAttackLeftSprite)
		{
			m_AttackLeftSprite = pAttackLeftSprite;
		}	
		inline void SetAttackRightSprite(Sprite* pAttackRightSprite)
		{
			m_AttackRightSprite = pAttackRightSprite;
		}	
		inline void SetCaptureSprite(Sprite* pCaptureSprite)
		{
			m_CaptureSprite = pCaptureSprite;
		}	
		inline void SetDeathSprite(Sprite* pDeathSprite)
		{
			m_DeathSprite = pDeathSprite;
		}	

	private:
		Sprite* m_ActiveSprite;

		Sprite* m_RightWalkSprite;
		Sprite* m_LeftWalkSprite;
		Sprite* m_AttackLeftSprite;
		Sprite* m_AttackRightSprite;
		Sprite* m_CaptureSprite;
		Sprite* m_DeathSprite;

		Movement m_MovementState;
		Jumping m_JumpState;
		Living m_LivingState;
	};
}
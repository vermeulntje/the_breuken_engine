#include "TheBreukenEnginePCH.h"
#include "FPSCounter.h"
#include "GameObject.h"

breuk::FPSCounter::FPSCounter()
	:breuk::BaseComponent()
	, m_Count{}
	, m_ElapsedTime{}
{
}
breuk::FPSCounter::~FPSCounter()
{
}

bool breuk::FPSCounter::Initialize()
{
	m_Initialised = true;
	return m_Initialised;
}

void breuk::FPSCounter::Update(const float deltaT)
{
	m_Count++;
	m_ElapsedTime += deltaT;
}

bool breuk::FPSCounter::CheckTime(int& count)
{
	if (m_ElapsedTime >= 1.0f) 
	{
		m_ElapsedTime -= 1.0f;
		count = m_Count;
		m_Count = 0;
		return true;
	}
	return false;
}

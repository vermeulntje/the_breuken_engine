#pragma once
#include "Singleton.h"

#include <unordered_map>
#include <XInput.h>

namespace breuk
{
	class BaseCommand;

	class InputManager final : public Singleton<InputManager>
	{
	public:
		InputManager();
		~InputManager();

		InputManager(InputManager& other) = delete;
		InputManager(InputManager&& other) = delete;
		InputManager operator=(InputManager& other) = delete;
		InputManager& operator=(InputManager&& other) = delete;

		bool ProcessInput();
		void HandleInput();

		void AddCommand(const int newKey, breuk::BaseCommand* pCommand, const int conrollerInput = 0);
		bool SwapCommand(const int newKey, const int prevKey);

	private:
		//Keyboard
		std::unordered_map<int, BaseCommand*> m_UMapCommands;
		std::unordered_map<int, BaseCommand*> m_UMapControllerCommands;

		//Controller
		std::vector<XINPUT_STATE> m_CurrentState;
		std::vector<bool> m_ControllerDetected;
		bool m_AnyControllerDetected;
	};
}

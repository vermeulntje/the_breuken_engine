#include "TheBreukenEnginePCH.h"
#include "TheBreukenEngine.h"
#include "Settings.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "Components.h"
#include "GameObject.h"
#include "Scene.h"

#include <chrono>
#include <thread>


using namespace std;
using namespace chrono;

void breuk::TheBreukenEngine::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw runtime_error(string("SDL_Init Error: ") + SDL_GetError());
	}

	m_Window = SDL_CreateWindow(
		"BreukenEngine - Vermeulen Sebastien",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		Settings::m_WindowWidth,
		Settings::m_WindowHeight,
		SDL_WINDOW_OPENGL
	);
	if (m_Window == nullptr) 
	{
		throw runtime_error(string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	breuk::Renderer::GetInstance().Init(m_Window);
}

void breuk::TheBreukenEngine::Cleanup()
{
	breuk::Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(m_Window);
	m_Window = nullptr;
	SDL_Quit();
}

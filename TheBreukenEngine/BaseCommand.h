#pragma once

namespace breuk
{
	class BaseCommand
	{
	public:
		explicit BaseCommand();
		virtual ~BaseCommand();

		BaseCommand(BaseCommand& other) = delete;
		BaseCommand(BaseCommand&& other) = delete;
		BaseCommand& operator=(BaseCommand& other) = delete;
		BaseCommand& operator=(BaseCommand&& other) = delete;

		virtual void Execute() = 0;

		inline virtual void SetKeyState(const bool state) { m_CurrTriggered = state; }

	protected:
		//2 bools to determin if key is held or pressed or released
		bool m_CurrTriggered;
		bool m_PrevTriggered;
	};
}
#pragma once

namespace breuk
{
	class GameObject;

	enum class Event 
	{
		LandingEvent,
		DmgEvent,
		GameOverEvent,
		KillEvent,
		LevelFinish,
		FallThroughEvent,
	};

	class Observer
	{
	public:
		Observer();
		virtual ~Observer();

		Observer(Observer& other) = delete;
		Observer(Observer&& other) = delete;
		Observer operator=(Observer& other) = delete;
		Observer& operator=(Observer&& other) = delete;

		virtual void OnNotify(breuk::GameObject* pObject, const breuk::Event event) = 0;
	};
}
